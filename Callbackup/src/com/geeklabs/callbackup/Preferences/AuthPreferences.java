package com.geeklabs.callbackup.Preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class AuthPreferences {

	private static final String USER_ACCOUNT = "None";
	private static final String SIGN_IN_STATUS = "false";
	private static final String ACCESS_TOKEN = "access_token";
	private static final String EXPIRATION_TIME = "token_expiration_perion";
	private static final String REFRESH_TOKEN = "refresh_token";
	private static final String SCOPE_STRING = "scope";
	private static final String USER_ID = "0";
	private static final String PIC_URL = "picUrl";
	private static final String USERNAME = "userName";
	private static final String EMAIL = "email";
	private static final String TAG = "AuthPreferences"; 
	private static final String CLOSE_POPUP = "closePopup";
	
	private SharedPreferences preferences;

	public AuthPreferences(Context context) {
		preferences = context.getSharedPreferences("Sms_and_call_history_preferences", Context.MODE_PRIVATE);
		Log.i(TAG, "Sms_and_call_history_preferences created!");
	}

	public void setTokenExpirationTime(long time) {
		Editor editor = preferences.edit();
		editor.putLong(EXPIRATION_TIME, time);
		editor.commit();
	}

	public long getTokenExpirationTime() {
		return preferences.getLong(EXPIRATION_TIME, 0);
	}

	public void setScope(String scope) {
		Editor editor = preferences.edit();
		editor.putString(SCOPE_STRING, scope);
		editor.commit();
	}

	public String getScope() {
		return preferences.getString(SCOPE_STRING, null);
	}

	public void setAccessToken(String accessToken) {
		Editor editor = preferences.edit();
		editor.putString(ACCESS_TOKEN, accessToken);
		editor.commit();
	}

	public String getAccessToken() {
		return preferences.getString(ACCESS_TOKEN, null);
	}

	public void setRefreshToken(String refreshToken) {
		Editor editor = preferences.edit();
		editor.putString(REFRESH_TOKEN, refreshToken);
		editor.commit();
	}

	public String getRefreshToken() {
		return preferences.getString(REFRESH_TOKEN, null);
	}

	public void setUserAccount(String userAccount) {
		Editor editor = preferences.edit();
		editor.putString(USER_ACCOUNT, userAccount);
		editor.commit();
	}

	public String getUserAccount() {
		return preferences.getString(USER_ACCOUNT, null);
	}

	public void setSignInStatus(boolean signInStatus) {
		Editor editor = preferences.edit();
		editor.putBoolean(SIGN_IN_STATUS, signInStatus);
		editor.commit();
	}

	public boolean isUserSignedIn() {
		return preferences.getBoolean(SIGN_IN_STATUS, false);
	}

	public void setPopupClosed(boolean closePopup) {
		Editor editor = preferences.edit();
		editor.putBoolean(CLOSE_POPUP, closePopup);
		editor.commit();
	}

	public boolean isPopupClosed() {
		return preferences.getBoolean(CLOSE_POPUP, false);
	}

	public void setUserId(long uId) {
		Editor editor = preferences.edit();
		editor.putLong(USER_ID, uId);
		editor.commit();
	}

	public long getUserId() {
		return preferences.getLong(USER_ID, 0);
	}

	public void setUserName(String userName) {
		Editor editor = preferences.edit();
		editor.putString(USERNAME, userName);
		editor.commit();
	}

	public String getUserName() {
		return preferences.getString(USERNAME, null);
	}

	public void setEmail(String email) {
		Editor editor = preferences.edit();
		editor.putString(EMAIL, email);
		editor.commit();
	}

	public String getEmail() {
		return preferences.getString(EMAIL, null);
	}
	
	public void setPicUrl(String picUrl) {
		Editor editor = preferences.edit();
		editor.putString(PIC_URL, picUrl);
		editor.commit();
	}

	public String getPicUrl() {
		return preferences.getString(PIC_URL, null);
	}
	
	public void clearCredentials() {
		Editor editor = preferences.edit();
		editor.remove(ACCESS_TOKEN);
		editor.remove(EXPIRATION_TIME);
		editor.remove(REFRESH_TOKEN);
		editor.remove(SCOPE_STRING);
		editor.remove(USER_ACCOUNT);
		editor.remove(PIC_URL);
		editor.remove(CLOSE_POPUP);
		
		editor.commit();
	}
}
