package com.geeklabs.callbackup.oauth;

import java.io.IOException;

import net.frakbot.accounts.chooser.AccountChooser;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.geeklabs.callbackup.MainActivity;
import com.geeklabs.callbackup.R;
import com.geeklabs.callbackup.Preferences.AuthPreferences;
import com.geeklabs.callbackup.communication.task.post.AuthenticationTask;
import com.geeklabs.callbackup.domain.User;

public class AuthActivity extends Activity {

	private AuthPreferences authPreferences;
	private AccountManager accountManager;
	private Account userAccount;
	private static final int AUTHORIZATION_CODE = 1993;
	private static final int ACCOUNT_CODE = 1601;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		accountManager = AccountManager.get(this);
		authPreferences = new AuthPreferences(this);

		// Action bar back ground color
		BitmapDrawable bitmap = (BitmapDrawable) getResources().getDrawable(R.drawable.steel_blue);
		bitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
		getActionBar().setBackgroundDrawable(bitmap);

		invalidateToken();
		chooseAccount();
	}

	private void chooseAccount() {
		// using https://github.com/frakbot/Android-AccountChooser for
		// compatibility with older devices
		Intent intent = AccountChooser.newChooseAccountIntent(null, null, new String[] { "com.google" }, true, null, null, null, null,
				getApplicationContext());

		startActivityForResult(intent, ACCOUNT_CODE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_OK) {
			if (requestCode == AUTHORIZATION_CODE) {
				requestToken();
			} else if (requestCode == ACCOUNT_CODE) {
				String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
				authPreferences.setUserAccount(accountName);

				// invalidate old tokens which might be cached. we want a fresh
				// one, which is guaranteed to work
				invalidateToken();
				requestToken();
			}
		} else if (resultCode == RESULT_CANCELED) {
			Intent i = new Intent(this, MainActivity.class);
			this.startActivity(i);
		}
	}

	private void requestToken() {
		String user = authPreferences.getUserAccount();
		for (Account account : accountManager.getAccountsByType("com.google")) {
			if (account.name.equals(user)) {
				userAccount = account;
				break;
			}
		}
		accountManager.getAuthToken(userAccount, "oauth2:" + OauthUserCredStore.SCOPE, null, this, new OnTokenAcquired(), null);
	}

	private class OnTokenAcquired implements AccountManagerCallback<Bundle> {

		@Override
		public void run(AccountManagerFuture<Bundle> result) {
			try {
				Bundle bundle = result.getResult();
				Intent launch = (Intent) bundle.get(AccountManager.KEY_INTENT);
				if (launch != null) {
					startActivityForResult(launch, AUTHORIZATION_CODE);
				} else {
					String token = bundle.getString(AccountManager.KEY_AUTHTOKEN);
					authPreferences.setAccessToken(token);
					invalidateToken();
					AsyncTask<Void, String, String> asyncTask = new AsyncTask<Void, String, String>() {

						@Override
						protected String doInBackground(Void... params) {
							return requestNewToken();
						}

					};
					asyncTask.execute();
					String newToken = asyncTask.get();
					// Register new user
					registerAndValidateUser(newToken);
				}

			} catch (OperationCanceledException canceledException) {
				// If user cancels operation, send back to sign in page
				Intent i = new Intent(AuthActivity.this, MainActivity.class);
				AuthActivity.this.startActivity(i);
				finish();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}

		}

	}

	private String requestNewToken() {
		AccountManagerFuture<Bundle> accountManagerFuture = accountManager.getAuthToken(userAccount, "oauth2:" + OauthUserCredStore.SCOPE, null,
				this, new OnNewTokenAccquired(), null);
		String newAuthToken = null;
		try {
			Bundle bundle = accountManagerFuture.getResult();
			newAuthToken = bundle.getString(AccountManager.KEY_AUTHTOKEN);
		} catch (OperationCanceledException e) {
			e.printStackTrace();
			// If user cancels operation, send back to sign in page
			Intent i = new Intent(AuthActivity.this, MainActivity.class);
			AuthActivity.this.startActivity(i);
		} catch (AuthenticatorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newAuthToken;

	}

	private void registerAndValidateUser(String accessToken) {
		final ProgressDialog loginIsInProgressDialog = new ProgressDialog(AuthActivity.this);
		loginIsInProgressDialog.setMessage("Login is in process...");
		Log.i("User authentication", "User authentication is in progress");
		loginIsInProgressDialog.setCancelable(false);
		loginIsInProgressDialog.show();

		// get the User to send to server
		User user = new User();
		user.setAccesstoken(accessToken);

		// call the aynctask to validate authtoken and user type
		AuthenticationTask validateUserRunnable = new AuthenticationTask(loginIsInProgressDialog, AuthActivity.this, user);
		validateUserRunnable.execute();
	}

	private void invalidateToken() {
		AccountManager accountManager = AccountManager.get(this);
		accountManager.invalidateAuthToken("com.google", authPreferences.getAccessToken());

		authPreferences.setAccessToken(null);
	}

}
