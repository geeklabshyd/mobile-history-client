package com.geeklabs.callbackup.oauth;

import com.geeklabs.callbackup.Preferences.AuthPreferences;


public final class OauthUserCredStore {
	public static final String CLIENT_ID = "494428155412-3lhr7po4nhcnv6dfu89bkb0o63c3hfop.apps.googleusercontent.com";
	public static final String CLIENT_SECRET = "98wzw9r8IGKyqUnuexEG-cPc";
	
	public static final String SCOPE = "https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile";
	public static final String REDIRECT_URI = "http://localhost";

	private final AuthPreferences prefs;

	private static OauthUserCredStore store;

	private OauthUserCredStore(AuthPreferences prefs) {
		this.prefs = prefs;
	}

	public static OauthUserCredStore getInstance(AuthPreferences prefs) {
		if (store == null)
			store = new OauthUserCredStore(prefs);

		return store;
	}

	public void clearCredentials() {
		prefs.clearCredentials();
	}

}
