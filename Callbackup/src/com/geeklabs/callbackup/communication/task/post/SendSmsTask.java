package com.geeklabs.callbackup.communication.task.post;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.JSONException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import com.geeklabs.callbackup.TabsActivity;
import com.geeklabs.callbackup.Preferences.AuthPreferences;
import com.geeklabs.callbackup.communication.AbstractHttpPostTask;
import com.geeklabs.callbackup.domain.SMS;
import com.geeklabs.callbackup.sqlite.SQLLiteManager;
import com.geeklabs.callbackup.util.RequestUrl;

public class SendSmsTask extends AbstractHttpPostTask {

	private Activity activity;
	private SMS sms;
	private AuthPreferences authPreferences;

	public SendSmsTask(ProgressDialog progressDialog, Activity context, SMS sms) {
		super(progressDialog, context);
		this.activity = context;
		this.sms = sms;
		authPreferences = new AuthPreferences(activity);
	}

	@Override
	protected String getRequestJSON() {
		ObjectMapper mapper = new ObjectMapper();
		List<SMS> list = new ArrayList<SMS>();
		try {
			return mapper.writeValueAsString(list.add(sms));
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected String getRequestUrl() {
		return RequestUrl.SEND_SMS_HISTORY_REQ;
	}

	@Override
	protected void showMessageOnUI(String msg) {
		Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		List<SMS> smses = new ArrayList<SMS>();
		SQLLiteManager sqlLiteManager = SQLLiteManager.getInstance(activity);
		if (!jsonResponse.isEmpty()) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				smses = mapper.readValue(jsonResponse, new TypeReference<List<SMS>>() {
				});

				if (!smses.isEmpty()) {
					// update server id
					for (SMS sms : smses) {
						Long serverSmsId = sms.getServerSmsId();
						if (serverSmsId != null && serverSmsId > 0) {
							SMS smsById = sqlLiteManager.getSmsById(sms.getId());
							if (smsById != null) {
								SQLLiteManager.getInstance(activity).updateOfflineSMSHistory(sms);
								TabsActivity.getMTabsAdaperToNofify().notifyDataSetChanged();
							}
						}
					}
				}
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
