package com.geeklabs.callbackup.communication.task.post;

import java.io.IOException;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;

import android.app.ProgressDialog;
import android.content.Context;

import com.geeklabs.callbackup.TabsActivity;
import com.geeklabs.callbackup.Preferences.AuthPreferences;
import com.geeklabs.callbackup.communication.AbstractHttpPostTask;
import com.geeklabs.callbackup.domain.DeletedRecords;
import com.geeklabs.callbackup.domain.SMS;
import com.geeklabs.callbackup.domain.Sync;
import com.geeklabs.callbackup.sqlite.SQLLiteManager;
import com.geeklabs.callbackup.util.RequestUrl;

public class SendSmsesTask extends AbstractHttpPostTask {

	private Context context;
	private AuthPreferences authPreferences;

	public SendSmsesTask(ProgressDialog progressDialog, Context context) {
		super(progressDialog, context);
		this.context = context;
		this.authPreferences = new AuthPreferences(context);
	}

	@Override
	protected String getRequestJSON() {
		List<SMS>  smses = SQLLiteManager.getInstance(context).getAllOfflineSmsesWithoutServerId();
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(smses);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected String getRequestUrl() {
		return RequestUrl.SEND_SMS_HISTORY_REQ.replace("{id}", String.valueOf(authPreferences.getUserId()));
	}

	@Override
	protected void showMessageOnUI(String string) {
		// no need to show any message bcz its background service
	}

	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		// set response in SQLLite table
		Sync sync = null;
		if (!jsonResponse.isEmpty()) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				sync = mapper.readValue(jsonResponse, Sync.class);
				if (sync != null) {

					List<SMS> smsList = sync.getSmses();
					for (SMS sms : smsList) {
						Long serverSmsId = sms.getServerSmsId();
						if (sms.getId() > 0 && serverSmsId != null && serverSmsId > 0) {
							SQLLiteManager.getInstance(context).updateOfflineSMSHistory(sms);
						}
					}

					/*// delete records
					List<DeletedRecords> deletedCallRecords = sync.getDeletedRecords();
					for (DeletedRecords deletedRecord : deletedCallRecords) {
						SQLLiteManager.getInstance(context).deleteOfflineSMSHistory(deletedRecord.getServerId());
					}*/
					
					TabsActivity.getMTabsAdaperToNofify().notifyDataSetChanged();
				}
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
