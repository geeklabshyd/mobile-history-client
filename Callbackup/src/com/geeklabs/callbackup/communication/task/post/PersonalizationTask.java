package com.geeklabs.callbackup.communication.task.post;

import java.io.IOException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.widget.Toast;

import com.geeklabs.callbackup.MainActivity;
import com.geeklabs.callbackup.TabsActivity;
import com.geeklabs.callbackup.Preferences.AuthPreferences;
import com.geeklabs.callbackup.Preferences.PerPreferences;
import com.geeklabs.callbackup.communication.AbstractHttpPostTask;
import com.geeklabs.callbackup.util.Personalize;
import com.geeklabs.callbackup.util.RequestUrl;
import com.geeklabs.callbackup.util.ResponseStatus;

public class PersonalizationTask extends AbstractHttpPostTask {

	private Activity context;
	private AuthPreferences authPreferences;
	private PerPreferences perPreferences;

	public PersonalizationTask(ProgressDialog progressDialog, Activity context) {
		super(progressDialog, context);
		this.context = context;
		authPreferences = new AuthPreferences(context);
		perPreferences = new PerPreferences(context);
	}

	@Override
	protected String getRequestJSON() {
		Personalize personalize = new Personalize();
		
		personalize.setAllPersonalizes(perPreferences.isAllPersonalized());
		personalize.setDialedCalls(perPreferences.isDialedCallStatus());
		personalize.setMissedCalls(perPreferences.isMissedCallStatus());
		personalize.setRecievedCalls(perPreferences.isReceivedCallStatus());
		personalize.setSms(perPreferences.isMessageStatus());
		personalize.setContacts(perPreferences.isContactsEnabled());
		personalize.setUserId(authPreferences.getUserId());
		
		
		long id = perPreferences.getId();
		if (id == 0) {
			personalize.setId(null);
		} else {
			personalize.setId(id);
		}
		personalize.setUserId(authPreferences.getUserId());
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(personalize);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected String getRequestUrl() {
		return RequestUrl.PERSONLIZE_REQ.replace("{id}", String.valueOf(authPreferences.getUserId()));
	}

	@Override
	protected void showMessageOnUI(String message) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {

		if (!jsonResponse.isEmpty()) {
			// success case
			ObjectMapper mapper = new ObjectMapper();
			ResponseStatus responseStatus = new ResponseStatus();
			try {
				responseStatus = mapper.readValue(jsonResponse.toString(), ResponseStatus.class);

			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			if (responseStatus.getStatus().equals("success")) {
				Intent intent = new Intent(context, TabsActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(intent);
				context.finish();
				showMessageOnUI("Successfully saved your Preferences!");
				
			} else {
				Intent intent = new Intent(context, MainActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(intent);
				context.finish();
				showMessageOnUI("Problem occured while saving...");
			}
		} else {
			// failure case
			Intent intent = new Intent(context, MainActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(intent);
			context.finish();
			showMessageOnUI("Unable to reach server...");
		}
	}

}
