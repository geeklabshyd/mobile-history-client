package com.geeklabs.callbackup.communication.task.post;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.widget.Toast;

import com.geeklabs.callbackup.MainActivity;
import com.geeklabs.callbackup.TabsActivity;
import com.geeklabs.callbackup.Preferences.AuthPreferences;
import com.geeklabs.callbackup.Preferences.PerPreferences;
import com.geeklabs.callbackup.communication.AbstractHttpPostTask;
import com.geeklabs.callbackup.domain.User;
import com.geeklabs.callbackup.service.SentSmsObserverService;
import com.geeklabs.callbackup.util.AlertDialogPopup;
import com.geeklabs.callbackup.util.Personalize;
import com.geeklabs.callbackup.util.RequestUrl;
import com.geeklabs.callbackup.util.SyncUtil;

public class AuthenticationTask extends AbstractHttpPostTask {
	private User user;
	private Activity context;
	private Intent intent;
	private AuthPreferences authPreferences;
	private PerPreferences perPreferences;

	public AuthenticationTask(ProgressDialog progressDialog, Activity context, User user) {
		super(progressDialog, context);
		this.context = context;
		this.user = user;
		authPreferences = new AuthPreferences(context);
		perPreferences = new PerPreferences(context);
	}

	@Override
	protected String getRequestJSON() {
		ObjectMapper mapper = new ObjectMapper();
		String loginReq = null;
		try {
			loginReq = mapper.writeValueAsString(user);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return loginReq;
	}

	@Override
	protected String getRequestUrl() {
		return RequestUrl.VALIDATE_TOKEN;
	}

	@Override
	protected void showMessageOnUI(final String message) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		if (!jsonResponse.isEmpty()) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				user = mapper.readValue(jsonResponse.toString(), User.class);
				if (user != null) {
					String status = user.getStatus();

					if (status.equalsIgnoreCase("signIn")) {

						// create alert dialog
						AlertDialog alertDialog = AlertDialogPopup.showAlertDialog("Login issue",
										"With this credentials some one already logged in. Reset your device login status from web application (http://smsandcallhistory.appspot.com)!",
										null, "Ok", context,
										MainActivity.class, null);
						// show it
						alertDialog.show();
					} else if (status.equalsIgnoreCase("success")) {
						// Register for out sms 
						Intent listenSentSmsService = new Intent(context, SentSmsObserverService.class);
						context.startService(listenSentSmsService);
						
						// set properties
						setUserProperties(user);

						// Set User Personlized info
						setPersonlizedUserinfo(user);
						
						//show popup for Syncnow sync later
						Builder alertDialogBuilder = AlertDialogPopup.getAlertBuilder("Sync", "Sync all your SMS, Calls and Contacts", context);
						alertDialogBuilder.create();
						onSelectOnAlertDilogBuilder(alertDialogBuilder);
						alertDialogBuilder.show();
						
					} else {
						showMessageOnUI("Problem occurred while handling req...");
						intent = new Intent(context, MainActivity.class);
						context.startActivity(intent);
						context.finish();
					}
				} else {
					showMessageOnUI("With this email already exist...");
					intent = new Intent(context, MainActivity.class);
					context.startActivity(intent);
					context.finish();
				}
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				cancelDialog();
			}
		} else {
			showMessageOnUI("Try after some time...");
			intent = new Intent(context, MainActivity.class);
			context.startActivity(intent);
			context.finish();
		}
	}

	private void onSelectOnAlertDilogBuilder(Builder alertDialogBuilder) {
		
		alertDialogBuilder.setCancelable(false).setPositiveButton("Sync now", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				//on click on sync now show again popup call sync util
				SyncUtil syncUtil = new SyncUtil();
				syncUtil.syncNow(context, true);
			}
		});
		
		alertDialogBuilder.setNegativeButton("Sync later", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				triggerTabsActivity();
			}
		});
	}
	
	private final void triggerTabsActivity() {
		// Show home tabs activity
		intent = new Intent(context, TabsActivity.class);
		context.startActivity(intent);
		context.finish();
	}

	private void setPersonlizedUserinfo(User user2) {
		Personalize personalize = user2.getPersonaizeDto();
		if (personalize.getId() != null && personalize.getId() > 0) {
			perPreferences.setId(personalize.getId());
		}
		perPreferences.setAllPersonizeStatus(personalize.isAllPersonalizes());
		perPreferences.setDialedCallStatus(personalize.isDialedCalls());
		perPreferences.setMessageStatus(personalize.isSms());
		perPreferences.setMissedCallStatus(personalize.isMissedCalls());
		perPreferences.setReceivedCallStatus(personalize.isRecievedCalls());
	}

	private void setUserProperties(User user) {
		// Set user signIn status
		authPreferences.setSignInStatus(true);
		// Set user accessToken in status
		authPreferences.setAccessToken(this.user.getAccesstoken());
		// Set user userId in status
		authPreferences.setUserId(user.getId());
		// set email
		authPreferences.setUserAccount(user.getEmail());
		// set pic url
		authPreferences.setPicUrl(user.getPicUrl());
		// set user name
		authPreferences.setUserName(user.getUserName());
		authPreferences.setEmail(user.getEmail());
	}
}