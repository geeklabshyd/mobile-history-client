package com.geeklabs.callbackup.communication.task.post;

import java.io.IOException;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;

import android.app.Activity;
import android.app.ProgressDialog;

import com.geeklabs.callbackup.Preferences.AuthPreferences;
import com.geeklabs.callbackup.communication.AbstractHttpPostTask;
import com.geeklabs.callbackup.domain.CallHistory;
import com.geeklabs.callbackup.domain.DeletedRecords;
import com.geeklabs.callbackup.domain.SMS;
import com.geeklabs.callbackup.domain.Sync;
import com.geeklabs.callbackup.domain.SyncPreferences;
import com.geeklabs.callbackup.sqlite.SQLLiteManager;
import com.geeklabs.callbackup.util.RequestUrl;

public class SyncNowTask extends AbstractHttpPostTask {

	private Activity activity;
	private AuthPreferences authPreferences;
	private boolean isAuthFlow;
	private SyncPreferences syncPreferences;
	private boolean stopSync;

	public SyncNowTask(Activity context, ProgressDialog progressDialog, boolean isAuthFlow, SyncPreferences syncPreferences) {
		super(progressDialog, context);
		this.activity = context;
		this.isAuthFlow = isAuthFlow;
		authPreferences = new AuthPreferences(context.getApplicationContext());
		this.syncPreferences = syncPreferences;
	}

	@Override
	protected void cancelDialog() {
		if (stopSync) {
			super.cancelDialog();
		}
	}
	
	@Override
	protected void showMessageOnUI(String string) {
		// no need to show msg
	}

	@Override
	protected String getRequestUrl() {
		return RequestUrl.SYNC_NOW.replace("{id}", String.valueOf(authPreferences.getUserId()));
	}

	@Override
	protected String getRequestJSON() {

		ObjectMapper mapper = new ObjectMapper();
		try {
			// get the data from sqlite with pagable
			Sync sync = new Sync();
			if (syncPreferences.isSyncAll() || syncPreferences.isSyncCall()) {
				//get calls by pagable
				List<CallHistory> callHistories = SQLLiteManager.getInstance(activity).getOfflineCallsWithoutServerIdByPagable(0);
				sync.setCallLogs(callHistories);
			}
			
			if (syncPreferences.isSyncAll() || syncPreferences.isSyncSms()) {
				List<SMS> smses = SQLLiteManager.getInstance(activity).getOfflineSmsesWithoutServerIdByPagable(0);
				sync.setSmses(smses);
			}
			
			sync.setAllSynced(syncPreferences.isSyncAll());
			sync.setCallsSynced(syncPreferences.isSyncCall());
			sync.setContactsSynced(syncPreferences.isSyncContacts());
			sync.setSmsSynced(syncPreferences.isSyncSms());
			
			if (sync.getCallLogs().isEmpty() && sync.getSmses().isEmpty()) {
				stopSync = true;
			}
			
			return mapper.writeValueAsString(sync);
		} catch (Exception e) {
			showMessageOnUI("problem occured while phoneBook in");
			throw new IllegalStateException("problem occured json parsing while phoneBook in");
		}
	}

	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		// set response in SQLLite table
		Sync sync = new Sync();
		SQLLiteManager sqlLiteManager = SQLLiteManager.getInstance(activity);

		if (!jsonResponse.isEmpty()) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				sync = mapper.readValue(jsonResponse, Sync.class);

				// update with local calls
				List<CallHistory> callHistories = sync.getCallLogs();
				if (!callHistories.isEmpty()) {
					for (CallHistory callHistory : callHistories) {
						// update call if exist
						CallHistory callHistoryById = sqlLiteManager.getCallHistoryById(callHistory.getId());
						if (callHistoryById != null) {
							sqlLiteManager.updateOfflineCallHistory(callHistory);
						} 
					}
				}

				// get all sms list and update
				List<SMS> smses = sync.getSmses();
				if (!smses.isEmpty()) {
					for (SMS sms : smses) {
						SMS smsById = sqlLiteManager.getSmsById(sms.getId());
						if (smsById != null) {
							sqlLiteManager.updateOfflineSMSHistory(sms);
						} 
					}
				}
				
				// delete records
				List<DeletedRecords> deletedCallRecords = sync.getDeletedRecords();

				if (!deletedCallRecords.isEmpty()) {
					for (DeletedRecords deletedRecord : deletedCallRecords) {
						sqlLiteManager.deleteOfflineCallHistory(deletedRecord.getServerId());
					}
				}

				// delete records
				List<DeletedRecords> deletedSmsRecords = sync.getDeletedRecords();
				for (DeletedRecords deletedRecord : deletedSmsRecords) {
					sqlLiteManager.deleteOfflineSMSHistory(deletedRecord.getServerId());
				}
				
				if (stopSync) {
					// Stop server communication
					/*if (isAuthFlow) {
						// Show home tabs activity
						Intent intent = new Intent(activity, TabsActivity.class);
						activity.startActivity(intent);
						activity.finish();
					}*/
				} else {
					//call this async task
					SyncNowTask syncNowTask = new SyncNowTask(activity, progressDialog, isAuthFlow, syncPreferences);
					syncNowTask.execute();
				}
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} 
		}
	}
}
