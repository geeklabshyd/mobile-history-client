package com.geeklabs.callbackup.communication.task.post;

import java.io.IOException;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;

import android.app.ProgressDialog;
import android.content.Context;

import com.geeklabs.callbackup.Preferences.AuthPreferences;
import com.geeklabs.callbackup.communication.AbstractHttpPostTask;
import com.geeklabs.callbackup.domain.CallHistory;
import com.geeklabs.callbackup.domain.DeletedRecords;
import com.geeklabs.callbackup.domain.Sync;
import com.geeklabs.callbackup.sqlite.SQLLiteManager;
import com.geeklabs.callbackup.util.RequestUrl;

public class SendCallsTask extends AbstractHttpPostTask {

	private Context context;
	private AuthPreferences authPreferences;

	public SendCallsTask(Context context, ProgressDialog progressDialog) {
		super(progressDialog, context);
		this.context = context;
		authPreferences = new AuthPreferences(context);
	}

	@Override
	protected String getRequestJSON() {
		List<CallHistory> callHistories = SQLLiteManager.getInstance(context).getAllOfflineCallsWithoudServerCallId();
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(callHistories);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected String getRequestUrl() {
		return RequestUrl.SEND_CALL_HISTORY_REQ.replace("{id}",
				String.valueOf(authPreferences.getUserId()));
	}

	@Override
	protected void showMessageOnUI(String string) {
		// no need to show any msg to user this req came from service
	}

	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		// set response in SQLLite table
		Sync sync = null;
		if (!jsonResponse.isEmpty()) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				sync = mapper.readValue(jsonResponse, Sync.class);
				if (sync != null) {
					List<CallHistory> callHistories = sync.getCallLogs();
					if (!callHistories.isEmpty()) {
						for (CallHistory callHistory : callHistories) {
							Long serverCallId = callHistory.getServerCallId();
							if (callHistory.getId() > 0 && serverCallId != null && serverCallId > 0) {
								SQLLiteManager.getInstance(context).updateOfflineCallHistory(callHistory);
							}
						}
					}

					// delete records
					List<DeletedRecords> deletedCallRecords = sync.getDeletedRecords();

					if (!deletedCallRecords.isEmpty()) {
						for (DeletedRecords deletedRecord : deletedCallRecords) {
							SQLLiteManager.getInstance(context).deleteOfflineCallHistory(deletedRecord.getServerId());
						}
					}
				}
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
