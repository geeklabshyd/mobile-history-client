package com.geeklabs.callbackup.communication.task.post;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.JSONException;

import android.app.Activity;

import com.geeklabs.callbackup.Preferences.AuthPreferences;
import com.geeklabs.callbackup.communication.AbstractHttpPostTask;
import com.geeklabs.callbackup.domain.Contact;
import com.geeklabs.callbackup.domain.SyncPreferences;
import com.geeklabs.callbackup.util.Contacts;
import com.geeklabs.callbackup.util.RequestUrl;

public class SyncContactsTask extends AbstractHttpPostTask {

	private Activity context;
	private AuthPreferences authPreferences;
	private SyncPreferences syncPreferences;

	public SyncContactsTask(Activity context, SyncPreferences syncPreferences) {
		super(null, context);
		this.context = context;
		this.syncPreferences = syncPreferences;
		this.authPreferences = new AuthPreferences(context);
	}

	@Override
	protected String getRequestJSON() {
		List<Contact> phoneBook = Contacts.getAllContactsFromPhoneBook(context);

		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(phoneBook);
		} catch (Exception e) {
			throw new IllegalStateException("problem occured json parsing while phoneBook in");
		}
	}

	@Override
	protected String getRequestUrl() {
		return RequestUrl.SEND_PHONE_BOOK.replace("{id}", String.valueOf(authPreferences.getUserId()));
	}

	@Override
	protected void showMessageOnUI(String string) {
	}

	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		if (!jsonResponse.isEmpty()) {
			ObjectMapper objectMapper = new ObjectMapper();
			try {
				List<Contact> contacts = new ArrayList<Contact>();
				contacts = objectMapper.readValue(jsonResponse, new TypeReference<List<Contact>>() {
				});
				// Update Phone Contacts Test
				if (!contacts.isEmpty()) {
					Contacts.addContactToAPhoneBook(contacts, context);
				}
				syncPreferences.getProgressStatus().setSyncContacts(true);
				syncPreferences.closeSpinnerIfAllTasksAreDone(context);
			} catch (Exception e) {
				e.printStackTrace();
				syncPreferences.getProgressStatus().setSyncContacts(true);
				syncPreferences.closeSpinnerIfAllTasksAreDone(context);
			} 
		}
	}
}
