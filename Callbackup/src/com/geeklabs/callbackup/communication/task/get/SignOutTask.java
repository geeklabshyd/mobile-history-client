package com.geeklabs.callbackup.communication.task.get;

import java.io.IOException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.geeklabs.callbackup.MainActivity;
import com.geeklabs.callbackup.Preferences.AuthPreferences;
import com.geeklabs.callbackup.Preferences.PerPreferences;
import com.geeklabs.callbackup.communication.AbstractHttpGetTask;
import com.geeklabs.callbackup.sqlite.SQLLiteManager;
import com.geeklabs.callbackup.util.RequestUrl;
import com.geeklabs.callbackup.util.ResponseStatus;

public class SignOutTask extends AbstractHttpGetTask {

	private Context context;
	private AuthPreferences authPreferences;
	private Activity activity;
	private PerPreferences perPreferences;

	public SignOutTask(ProgressDialog progressDialog, Context context, Activity activity) {
		super(progressDialog, context);
		this.context = context;
		authPreferences = new AuthPreferences(context);
		perPreferences = new PerPreferences(context);
		this.activity = activity;
	}

	@Override
	protected String getRequestUrl() {

		String id = String.valueOf(authPreferences.getUserId());
		return RequestUrl.SIGNOUT.replace("{id}", id);
	}

	@Override
	protected void showMessageOnUI(final String message) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		if (!jsonResponse.isEmpty()) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				ResponseStatus responseStatus = mapper.readValue(jsonResponse, ResponseStatus.class);
				String status = responseStatus.getStatus();
				if (status.equals("success")) {
					// clear all preferences data
					clearPreferences();

					// delete all sqllite data
					// from call table
					SQLLiteManager.getInstance(context).deleteAllCalls();

					// from SMs table
					SQLLiteManager.getInstance(context).deleteAllSmses();

					Intent i = new Intent(context, MainActivity.class);
					i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(i);
					activity.finish();
					showMessageOnUI(" You are successfully logged out");
					cancelDialog();
				} else {
					showMessageOnUI("Unable to reach server, try after sometime");
					cancelDialog();
				}
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			showMessageOnUI("Unable to reach server, try after sometime");
			cancelDialog();
		}

	}

	private void clearPreferences() {
		// Set user signin status
		authPreferences.setSignInStatus(false);
		
		// Set user UserId status
		authPreferences.setUserId(0);
		perPreferences.setId(0);

		AccountManager accountManager = AccountManager.get(context);
		accountManager.invalidateAuthToken("com.google", authPreferences.getAccessToken());
		// Set user AccessToken status
		authPreferences.setAccessToken(null);

		
		// clear auth preferences
		authPreferences.clearCredentials();
		// clear per preferences
		perPreferences.clearPreferences();

	}

	@Override
	protected String getRequestJSON() {
		return "";
	}
}
