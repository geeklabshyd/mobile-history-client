package com.geeklabs.callbackup.communication.task.get;

import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.geeklabs.callbackup.domain.SMS;
import com.geeklabs.callbackup.sqlite.SQLLiteManager;

public abstract class GetOfflineSMSBaseTask extends AsyncTask<Void, ProgressDialog, List<SMS>>{

	private Activity activity;
	private int currentPage;
	private String searchTerm;
	public GetOfflineSMSBaseTask(Activity activity, int currentPage, String searchTerm) {
		this.activity = activity;
		this.currentPage = currentPage;
		this.searchTerm = searchTerm;
	}
	
	@Override
	protected void onPostExecute(List<SMS> smses) {
		if (smses != null && !smses.isEmpty()) {
			updateUI(smses);
		} else {
			stopProgressBar();
		}
	}
	
	@Override
	protected List<SMS> doInBackground(Void... params) {
		return SQLLiteManager.getInstance(activity.getApplicationContext()).getPaginationAllSmses(currentPage, searchTerm);
	}
	
	protected abstract void updateUI(List<SMS> smses);
	protected abstract void stopProgressBar();
}
