package com.geeklabs.callbackup.communication.task.get;

import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.geeklabs.callbackup.domain.CallHistory;
import com.geeklabs.callbackup.sqlite.SQLLiteManager;

public abstract class GetOfflineCallsByStatusBaseTask extends AsyncTask<Void, ProgressDialog, List<CallHistory>>{
	
	private Activity activity;
	private int currentPage;
	private int type;
	private String searchTerm;
	private ProgressDialog progress;

	public GetOfflineCallsByStatusBaseTask(Activity activity, ProgressDialog progressDialog, int currentPage, int type, String searchTerm) {
		this.activity = activity;
		this.currentPage = currentPage;
		this.type = type;
		this.searchTerm = searchTerm;
		this.progress = progressDialog;
	}
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		progress.show();
	}
	
	@Override
	protected void onPostExecute(List<CallHistory> callHistories) {
		if (callHistories != null && !callHistories.isEmpty()) {
			updateUI(callHistories);
			progress.cancel();
		} else {
			stopProgressBar();
			progress.cancel();
		}
	}
	
	@Override
	protected List<CallHistory> doInBackground(Void... params) {
		return SQLLiteManager.getInstance(activity.getApplicationContext()).getPaginationAllCallsByStatus(currentPage, type, searchTerm);
	}
	
	protected abstract void updateUI(List<CallHistory> callHistories);

	protected abstract void stopProgressBar();
	
	public void setSearchTerm(String searchTerm) {
		this.searchTerm = searchTerm;
	}
}
