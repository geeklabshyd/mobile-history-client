package com.geeklabs.callbackup.service;

import android.app.IntentService;
import android.content.Intent;

import com.geeklabs.callbackup.communication.AbstractHttpPostTask;
import com.geeklabs.callbackup.communication.task.post.SendCallsTask;
import com.geeklabs.callbackup.communication.task.post.SendSmsesTask;
import com.geeklabs.callbackup.util.NetworkService;

public class SendCallsAndSmsesService extends IntentService {

	public SendCallsAndSmsesService() {
		super("Send calls  to server");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// Send request to server if n/w available
		if (NetworkService.isSyncWifiOrDataEnabled(getApplicationContext())) {

			AbstractHttpPostTask sendCallsRequest = new SendCallsTask(getApplicationContext(), null);
			sendCallsRequest.execute();

			AbstractHttpPostTask sendSmsesRequest = new SendSmsesTask(null, getApplicationContext());
			sendSmsesRequest.execute();
		}
	}
}
