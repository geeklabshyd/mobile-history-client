package com.geeklabs.callbackup.service;

import android.app.IntentService;
import android.content.Intent;

import com.geeklabs.callbackup.communication.AbstractHttpPostTask;
import com.geeklabs.callbackup.communication.task.post.SendSmsesTask;
import com.geeklabs.callbackup.util.NetworkService;

public class SendSMSService extends IntentService   {

	public SendSMSService(){
		super("Send smses  to server");
	}
	@Override
	protected void onHandleIntent(Intent intent) {
		// Send request to server if n/w available
			if (NetworkService.isNetWorkAvailable(getApplicationContext())) {
				AbstractHttpPostTask sendCallsRequest = new SendSmsesTask(null, getApplicationContext());
				sendCallsRequest.execute();
			}
	}

}
