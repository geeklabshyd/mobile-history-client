package com.geeklabs.callbackup.service;

import com.geeklabs.callbackup.reciver.ReadSentSmsObserver;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;

public class SentSmsObserverService extends Service {

	private ReadSentSmsObserver readSentSmsObserver;

	public void onCreate() {        
        super.onCreate();  
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
    public int onStartCommand(Intent intent, int flags, int startId) {  
        super.onStartCommand(intent, flags, startId);           

        readSentSmsObserver = new ReadSentSmsObserver(getApplicationContext(), new Handler());
        getContentResolver().registerContentObserver(Uri.parse("content://sms"),true, readSentSmsObserver);
        
        return Service.START_STICKY;
    }
	
	@Override
    public void onDestroy() {
        getContentResolver().unregisterContentObserver(readSentSmsObserver);
        super.onDestroy();
    }
	
	@Override
    public boolean onUnbind(Intent intent) {
     return super.onUnbind(intent);
    }
}
