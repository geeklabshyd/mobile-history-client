package com.geeklabs.callbackup.domain;

import java.util.ArrayList;
import java.util.List;


public class Sync {
	private List<SMS> smses = new ArrayList<SMS>();
	private List<CallHistory>  callLogs = new ArrayList<CallHistory>();  
	private List<DeletedRecords> deletedRecords = new  ArrayList<DeletedRecords>();
	
	private boolean isAllSynced;
	private boolean isSmsSynced;
	private boolean isCallsSynced;
	private boolean isContactsSynced;
	
	public boolean isAllSynced() {
		return isAllSynced;
	}
	public void setAllSynced(boolean isAllSynced) {
		this.isAllSynced = isAllSynced;
	}
	public boolean isSmsSynced() {
		return isSmsSynced;
	}
	public void setSmsSynced(boolean isSmsSynced) {
		this.isSmsSynced = isSmsSynced;
	}
	public boolean isCallsSynced() {
		return isCallsSynced;
	}
	public void setCallsSynced(boolean isCallsSynced) {
		this.isCallsSynced = isCallsSynced;
	}
	public boolean isContactsSynced() {
		return isContactsSynced;
	}
	public void setContactsSynced(boolean isContactsSynced) {
		this.isContactsSynced = isContactsSynced;
	}
	public List<SMS> getSmses() {
		return smses;
	}
	public void setSmses(List<SMS> smses) {
		this.smses = smses;
	}
	public List<DeletedRecords> getDeletedRecords() {
		return deletedRecords;
	}
	public void setDeletedRecords(List<DeletedRecords> deletedRecords) {
		this.deletedRecords = deletedRecords;
	}
	public List<CallHistory> getCallLogs() {
		return callLogs;
	}
	public void setCallLogs(List<CallHistory> callLogs) {
		this.callLogs = callLogs;
	}
}
