package com.geeklabs.callbackup.domain;

import android.app.Activity;

public class SyncPreferences {

	private boolean isSyncSms;
	private boolean isSyncCall;
	private boolean isSyncContacts;
	private boolean isSyncAll = true;
	private SyncProgressStatus progressStatus = new SyncProgressStatus();

	public void closeSpinnerIfAllTasksAreDone(Activity context) {
		if (progressStatus.isSyncCall() && progressStatus.isSyncContacts() && progressStatus.isSyncSms()) {
			context.setProgressBarIndeterminateVisibility(false);
		}
	}

	public SyncProgressStatus getProgressStatus() {
		return progressStatus;
	}

	public boolean isSyncSms() {
		return isSyncSms;
	}

	public void setSyncSms(boolean isSyncSms) {
		this.isSyncSms = isSyncSms;
	}

	public boolean isSyncCall() {
		return isSyncCall;
	}

	public void setSyncCall(boolean isSyncCall) {
		this.isSyncCall = isSyncCall;
	}

	public boolean isSyncContacts() {
		return isSyncContacts;
	}

	public void setSyncContacts(boolean isSyncContacts) {
		this.isSyncContacts = isSyncContacts;
	}

	public boolean isSyncAll() {
		return isSyncAll;
	}

	public void setSyncAll(boolean isSyncAll) {
		this.isSyncAll = isSyncAll;
	}

}
