package com.geeklabs.callbackup.domain;

public class SyncProgressStatus {

	private boolean isSyncSms;
	private boolean isSyncCall;
	private boolean isSyncContacts;
	
	public boolean isSyncSms() {
		return isSyncSms;
	}
	public void setSyncSms(boolean isSyncSms) {
		this.isSyncSms = isSyncSms;
	}
	public boolean isSyncCall() {
		return isSyncCall;
	}
	public void setSyncCall(boolean isSyncCall) {
		this.isSyncCall = isSyncCall;
	}
	public boolean isSyncContacts() {
		return isSyncContacts;
	}
	public void setSyncContacts(boolean isSyncContacts) {
		this.isSyncContacts = isSyncContacts;
	}
}
