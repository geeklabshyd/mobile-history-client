package com.geeklabs.callbackup.domain;

public class Contact {

	private Long serverContactId;
	private String name;
	private String number;
	private Long userId;
	private boolean isSyncked;
	
	public boolean isSyncked() {
		return isSyncked;
	}
	public void setSyncked(boolean isSyncked) {
		this.isSyncked = isSyncked;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public Long getServerContactId() {
		return serverContactId;
	}
	public void setServerContactId(Long serverContactId) {
		this.serverContactId = serverContactId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
}
