package com.geeklabs.callbackup.domain.enums;

public enum UserStatus {
	NEW, ACTIVE, BANNED, BLOCKED, INACTIVE;
}
