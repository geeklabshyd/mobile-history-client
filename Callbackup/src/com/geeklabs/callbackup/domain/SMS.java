package com.geeklabs.callbackup.domain;

public class SMS {

	
	private int id;
	private Long serverSmsId;
	private Long userId;
	private String from;
	private String message;
	private String contactName;
	private String date;
	private boolean isSyncked;
	private boolean isDeleted;
	private int type;
	private String actualSmsType;
	
	public int getType() {
		return type;
	}
	
	public void setType(int type) {
		this.type = type;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}

	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public boolean isSyncked() {
		return isSyncked;
	}
	public void setSyncked(boolean isSyncked) {
		this.isSyncked = isSyncked;
	}
	public Long getServerSmsId() {
		return serverSmsId;
	}
	public void setServerSmsId(Long serverSmsId) {
		this.serverSmsId = serverSmsId;
	}
	public boolean isDeleted() {
		return isDeleted;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + id;
		result = prime * result + type;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SMS other = (SMS) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (id != other.id)
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public String getActualSmsType() {
		return actualSmsType;
	}
	
	public void setActualSmsType(String actualSmsType) {
		this.actualSmsType = actualSmsType;
	}
}
