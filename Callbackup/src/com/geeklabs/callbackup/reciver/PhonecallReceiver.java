package com.geeklabs.callbackup.reciver;

import java.util.Date;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;

public class PhonecallReceiver extends BroadcastReceiver {
	private static int lastState = TelephonyManager.CALL_STATE_IDLE;
    private static Date callStartTime;
    private static boolean isIncoming;
    private static String outCallingNumber;  //because the passed incoming is only valid in ringing
	/*private long start_time;
	private long end_time;
	private long total_time;*/
	@Override
	public void onReceive(Context context, Intent intent) {
		
		 /*if (intent.getAction().equalsIgnoreCase("android.intent.action.PHONE_STATE")) {
				if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(TelephonyManager.EXTRA_STATE_RINGING)) {
					start_time = System.currentTimeMillis();
				}
				if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(TelephonyManager.EXTRA_STATE_IDLE)) {
					end_time = System.currentTimeMillis();
					// Total time talked =
					total_time = end_time - start_time;
					// Store total_time somewhere or pass it to an Activity using
					// intent

				}

			}*/
		//We listen to two intents.  The new outgoing call only tells us of an outgoing call.  We use it to get the number.
        if (intent.getAction().equals("android.intent.action.NEW_OUTGOING_CALL")) {
            outCallingNumber = intent.getExtras().getString("android.intent.extra.PHONE_NUMBER");
        } else {
        	String stateStr = intent.getExtras().getString(TelephonyManager.EXTRA_STATE);
            String number = intent.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
            
            int state = 0;
            if(stateStr.equals(TelephonyManager.EXTRA_STATE_IDLE)){
                state = TelephonyManager.CALL_STATE_IDLE;
            }
            else if(stateStr.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)){
                state = TelephonyManager.CALL_STATE_OFFHOOK;
            }
            else if(stateStr.equals(TelephonyManager.EXTRA_STATE_RINGING)){
                state = TelephonyManager.CALL_STATE_RINGING;
            }
            
            onCallStateChanged(context, state, number);
		}
	}
	
	 //Deals with actual events
    //Incoming call-  goes from IDLE to RINGING when it rings, to OFFHOOK when it's answered, to IDLE when its hung up
    //Outgoing call-  goes from IDLE to OFFHOOK when it dials out, to IDLE when hung up
	public void onCallStateChanged(Context context, int state, String number) {
		 if(lastState == state){
	            //No change, debounce extras
	            return;
	        }
	        switch (state) {
	            case TelephonyManager.CALL_STATE_RINGING:
	                isIncoming = true;
	                callStartTime = new Date();
	                outCallingNumber = number;
	                onIncomingCallStarted(context, number, callStartTime);
	                break;
	            case TelephonyManager.CALL_STATE_OFFHOOK:
	                //Transition of ringing->offhook are pickups of incoming calls.  Nothing done on them
	                if(lastState != TelephonyManager.CALL_STATE_RINGING){
	                    isIncoming = false;
	                    callStartTime = new Date();
	                    onOutgoingCallStarted(context, outCallingNumber, callStartTime);                     
	                }
	                break;
	            case TelephonyManager.CALL_STATE_IDLE:
	                //Went to idle-  this is the end of a call.  What type depends on previous state(s)
	                if(lastState == TelephonyManager.CALL_STATE_RINGING){
	                    //Ring but no pickup-  a miss
	                    onMissedCall(context, outCallingNumber, callStartTime);
	                }
	                else if(isIncoming){
	                    onIncomingCallEnded(context, outCallingNumber, callStartTime, new Date());                       
	                }
	                else {
	                    onOutgoingCallEnded(context, outCallingNumber, callStartTime, new Date());                                               
	                }
	                break;
	        }
	        lastState = state;
	    }
	
	//Derived classes should override these to respond to specific events of interest
    protected void onIncomingCallStarted(Context ctx, String number, Date start){}
    protected void onOutgoingCallStarted(Context ctx, String number, Date start){}
    protected void onIncomingCallEnded(Context ctx, String number, Date start, Date end){}
    protected void onOutgoingCallEnded(Context ctx, String number, Date start, Date end){}
    protected void onMissedCall(Context ctx, String number, Date start){}
    /*protected String getCallDuration(){
    	return String.valueOf(total_time);
    }*/
}
