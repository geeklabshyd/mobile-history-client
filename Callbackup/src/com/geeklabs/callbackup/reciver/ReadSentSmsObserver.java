package com.geeklabs.callbackup.reciver;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;

import com.geeklabs.callbackup.Preferences.AuthPreferences;
import com.geeklabs.callbackup.Preferences.PerPreferences;
import com.geeklabs.callbackup.communication.task.post.SendSmsesTask;
import com.geeklabs.callbackup.domain.SMS;
import com.geeklabs.callbackup.sqlite.SQLLiteManager;
import com.geeklabs.callbackup.util.Constants;
import com.geeklabs.callbackup.util.Contacts;
import com.geeklabs.callbackup.util.NetworkService;

// http://stackoverflow.com/questions/2584058/android-querying-the-sms-contentprovider
public class ReadSentSmsObserver extends ContentObserver {

	private Context context;
	private PerPreferences perPreferences;

	public ReadSentSmsObserver(Context context2, Handler handler) {
		super(handler);
		this.context = context2;
		perPreferences = new PerPreferences(context2);
	}

	@Override
	public boolean deliverSelfNotifications() {
		return true;
	}
	
	@Override
	public void onChange(boolean selfChange) {
		super.onChange(selfChange);
		PerPreferences perPreferences = new PerPreferences(context);
		if (perPreferences.isMessageStatus() || perPreferences.isAllPersonalized()) {
			AuthPreferences authPreferences = new AuthPreferences(context);
			// save the message to the SD card here
			Uri uriSMSURI = Uri.parse("content://sms");
			Cursor cur = context.getContentResolver().query(uriSMSURI, null, null, null, null);
			// this will make it point to the first record, which is the last
			// SMS
			// sent
			while (cur.moveToNext()) {
				String type = cur.getString(cur.getColumnIndex("type"));
				// It contains a long indicating whether you are dealing with a received (type == 1) or sent (type == 2) message.
				if (Integer.parseInt(type) == 2) {
					String content = cur.getString(cur.getColumnIndex("body"));
					String address = cur.getString(cur.getColumnIndex("address"));
					Date now = new Date(cur.getLong(cur.getColumnIndex("date")));
					SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_SAVE_FORMAT);
					String formatedDate = format.format(now);
					
					SMS sms = new SMS();
					sms.setFrom(address);
					sms.setMessage(content);
					sms.setDate(formatedDate);
					sms.setType(Constants.SMS_DELIVERED);
					String contactName = Contacts.getContactName(context, address);
					sms.setContactName(contactName);

					if (authPreferences.isUserSignedIn() && authPreferences.getUserAccount() != null && authPreferences.getUserId() > 0) {
						// save sms
						SQLLiteManager.getInstance(context).saveOfflineSMS(sms);
						sendSmsesIfWi_fiOrDataEnable(context);
					}
					break;
				}
			}
			cur.close();
		}
	}
	private void sendSmsesIfWi_fiOrDataEnable(Context ctx) {
		// check N/w available
		if (NetworkService.isSyncWifiOrDataEnabled(ctx)) {
				SendSmsesTask sendSmsesTask = new SendSmsesTask(null, ctx);
				sendSmsesTask.execute();
		}
	}
}
