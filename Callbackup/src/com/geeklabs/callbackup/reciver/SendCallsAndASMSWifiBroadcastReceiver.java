package com.geeklabs.callbackup.reciver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.geeklabs.callbackup.Preferences.AuthPreferences;
import com.geeklabs.callbackup.service.SendCallsAndSmsesService;

public class SendCallsAndASMSWifiBroadcastReceiver extends BroadcastReceiver {
	private AuthPreferences authPreferences;
	@Override
	public void onReceive(Context context, Intent intent) {
		authPreferences = new AuthPreferences(context);
		int wifiState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE,
				WifiManager.WIFI_STATE_UNKNOWN);

		if (authPreferences.isUserSignedIn() && authPreferences.getUserId() > 0) {

			switch (wifiState) {
			case WifiManager.WIFI_STATE_ENABLED:
				Log.i("CallBackup: System event type", intent.getAction());
				// Call send trackings to server service
				Intent sendCallsService = new Intent(context, SendCallsAndSmsesService.class);
				context.startService(sendCallsService);

				break;
			default:
				break;
			}
		}
	}
}
