package com.geeklabs.callbackup.reciver;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

import com.geeklabs.callbackup.Preferences.AuthPreferences;
import com.geeklabs.callbackup.Preferences.PerPreferences;
import com.geeklabs.callbackup.communication.task.post.SendSmsesTask;
import com.geeklabs.callbackup.domain.SMS;
import com.geeklabs.callbackup.sqlite.SQLLiteManager;
import com.geeklabs.callbackup.util.Constants;
import com.geeklabs.callbackup.util.Contacts;
import com.geeklabs.callbackup.util.NetworkService;

public class SMSReciever extends BroadcastReceiver {

	private PerPreferences perPreferences;
	


	@Override
	public void onReceive(Context context, Intent intent) {
		
		perPreferences = new PerPreferences(context);
		// Send SMS only if user interested in!
		if (perPreferences.isMessageStatus() || perPreferences.isAllPersonalized()) {
			AuthPreferences authPreferences = new AuthPreferences(context);

			Bundle myBundle = intent.getExtras();
			android.telephony.SmsMessage[] messages = null;
			if (myBundle != null) {
				Object[] pdus = (Object[]) myBundle.get("pdus");
				messages = new android.telephony.SmsMessage[pdus.length];
				for (int i = 0; i < messages.length; i++) {
					messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
					String smsFrom = messages[i].getOriginatingAddress();
					String msgBody = messages[i].getMessageBody();
					try {
						SMS sms = new SMS();
						sms.setFrom(smsFrom);
						sms.setMessage(msgBody);
						SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_SAVE_FORMAT);
						String formatedDate = format.format(new Date());
						sms.setDate(formatedDate);
						sms.setType(Constants.SMS_RECEIVED);
						sms.setUserId(authPreferences.getUserId());
						String contactName = Contacts.getContactName(context, smsFrom);
						sms.setContactName(contactName);

						if (authPreferences.isUserSignedIn() && authPreferences.getUserAccount() != null && authPreferences.getUserId() > 0) {
							//save sms
							SQLLiteManager.getInstance(context).saveOfflineSMS(sms);
							sendSmsesIfWi_fiOrDataEnable(context);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	
	private void sendSmsesIfWi_fiOrDataEnable(Context ctx) {
		// check N/w available
		
		if (NetworkService.isSyncWifiOrDataEnabled(ctx)) {
				SendSmsesTask sendSmsesTask = new SendSmsesTask(null, ctx);
				sendSmsesTask.execute();
		}
	}
}