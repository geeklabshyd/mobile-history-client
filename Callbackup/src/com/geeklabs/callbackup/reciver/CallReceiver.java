package com.geeklabs.callbackup.reciver;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Context;

import com.geeklabs.callbackup.Preferences.AuthPreferences;
import com.geeklabs.callbackup.Preferences.PerPreferences;
import com.geeklabs.callbackup.communication.task.post.SendCallsTask;
import com.geeklabs.callbackup.domain.CallHistory;
import com.geeklabs.callbackup.listfragment.tab.CallsTab;
import com.geeklabs.callbackup.sqlite.SQLLiteManager;
import com.geeklabs.callbackup.util.Constants;
import com.geeklabs.callbackup.util.Contacts;
import com.geeklabs.callbackup.util.NetworkService;

public class CallReceiver extends PhonecallReceiver {

	private AuthPreferences authPreferences;
	private PerPreferences perPreferences;
	CallHistory call = new CallHistory();

	@Override
	protected void onMissedCall(Context ctx, String number, Date start) {
		perPreferences = new PerPreferences(ctx);

		// Send SMS only if user interested in!
		if (perPreferences.isMissedCallStatus() || perPreferences.isAllPersonalized()) {
			String contactName = Contacts.getContactName(ctx, number);
			authPreferences = new AuthPreferences(ctx);
			call.setNumber(number);
			call.setUserId(authPreferences.getUserId());
			call.setCallType(Constants.MISSED);
			call.setTime(String.valueOf(start.getTime()));
			call.setContactName(contactName);

			SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_SAVE_FORMAT); // "dd/MM/yy:HH:mm:SS"
			String formatedDate = format.format(new Date());
			call.setDate(formatedDate);

			// save missed calls history in CallHistory SQLlite Table
			if (authPreferences.isUserSignedIn() && authPreferences.getUserAccount() != null && authPreferences.getUserId() > 0) {
				SQLLiteManager.getInstance(ctx).saveOfflineCallHistory(call);
				sendCallsIfWi_fiOrDataEnable(ctx);
			}
		}

	}


	@Override
	protected void onIncomingCallEnded(Context ctx, String number, Date start, Date end) {
		PerPreferences perPreferences = new PerPreferences(ctx);
		// Send SMS only if user interested in!
		if (perPreferences.isReceivedCallStatus() || perPreferences.isAllPersonalized()) {
			authPreferences = new AuthPreferences(ctx);
			String contactName = Contacts.getContactName(ctx, number);
			call.setNumber(number);
			call.setUserId(authPreferences.getUserId());
			call.setCallType(Constants.INCOMING);
			call.setContactName(contactName);
			SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_SAVE_FORMAT);
			call.setTime(String.valueOf(start.getTime()));
			String formatedDate = format.format(new Date());
			call.setDate(formatedDate);

			if (authPreferences.isUserSignedIn() && authPreferences.getUserAccount() != null && authPreferences.getUserId() > 0) {

				SQLLiteManager.getInstance(ctx).saveOfflineCallHistory(call);
				// TODO
				sendCallsIfWi_fiOrDataEnable(ctx);
			}
		}
	}
	
	@Override
	protected void onOutgoingCallEnded(Context ctx, String number, Date start, Date end) {
		PerPreferences perPreferences = new PerPreferences(ctx);
		// Send SMS only if user interested in!
		if (perPreferences.isDialedCallStatus() || perPreferences.isAllPersonalized()) {
			authPreferences = new AuthPreferences(ctx);
			String contactName = Contacts.getContactName(ctx, number);
			call.setNumber(number);
			call.setUserId(authPreferences.getUserId());
			call.setCallType(Constants.OUTGOING);
			call.setContactName(contactName);
			call.setTime(String.valueOf(start.getTime()));
			SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_SAVE_FORMAT);
			String formatedDate = format.format(new Date());
			call.setDate(formatedDate);
			if (authPreferences.isUserSignedIn() && authPreferences.getUserAccount() != null && authPreferences.getUserId() > 0) {

				SQLLiteManager.getInstance(ctx).saveOfflineCallHistory(call);
				sendCallsIfWi_fiOrDataEnable(ctx);
			}
		}
	}

	/*@Override
	protected String getCallDuration() {
		return super.getCallDuration();
	}*/
	
	
	private void sendCallsIfWi_fiOrDataEnable(Context ctx) {
		boolean enabled = NetworkService.isSyncWifiOrDataEnabled(ctx);
		if (enabled) {
			SendCallsTask sendCallsTask = new SendCallsTask(ctx, null);
			sendCallsTask.execute();
		}
	}
}