package com.geeklabs.callbackup.util;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.PhoneLookup;

import com.geeklabs.callbackup.domain.Contact;

public class Contacts {

	private Contacts() {
	}

	public static List<Contact> getAllContactsFromPhoneBook(Context context) {
		List<Contact> allContacts = new ArrayList<Contact>();

		ContentResolver cr = context.getContentResolver();
		Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
		if (cur.getCount() > 0) {
			while (cur.moveToNext()) {
				String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
				String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
				String phoneNo = null;
				if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
					Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
							ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[] { id }, null);
					while (pCur.moveToNext()) {
						phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
					}
					pCur.close();
				}

				if (phoneNo != null && !phoneNo.isEmpty()) {
					Contact contact = new Contact();
					contact.setName(name);
					contact.setNumber(phoneNo);

					allContacts.add(contact);
				}

			}
		}
		return allContacts;
	}

	public static String getContactName(Context context, String number) {
		String name = "UnKnown";
		// define the columns I want the query to return
		String[] projection = new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID };
		// encode the phone number and build the filter URI
		Uri contactUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
		// query time
		Cursor cursor = context.getContentResolver().query(contactUri, projection, null, null, null);
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				name = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
			} else {
				// Toast.makeText(context, "Contact name not found for the  " +
				// number, Toast.LENGTH_LONG).show();
			}
			cursor.close();
		}
		return name;
	}

	public boolean contactExists(Activity _activity, String number) {
		if (number != null) {
			Uri lookupUri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
			String[] mPhoneNumberProjection = { PhoneLookup._ID, PhoneLookup.NUMBER, PhoneLookup.DISPLAY_NAME };
			Cursor cur = _activity.getContentResolver().query(lookupUri, mPhoneNumberProjection, null, null, null);
			try {
				if (cur.moveToFirst()) {
					return true;
				}
			} finally {
				if (cur != null)
					cur.close();
			}
			return false;
		} else {
			return false;
		}
	}//

	public static void addContactToAPhoneBook(List<Contact> contacts, Context context) {

		for (Contact contact : contacts) {
			String contactName = getContactName(context, contact.getNumber());
			if (contactName == null || contactName.equalsIgnoreCase("UnKnown")) {
				contactName = contact.getName();
				ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

				ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
						.withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null).withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
						.build());

				// ------------------------------------------------------ Names
				if (contactName != null) {
					ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
							.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
							.withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
							.withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, contactName).build());
				}

				// ------------------------------------------------------ Mobile
				// Number
				String number = contact.getNumber();
				if (number != null) {
					ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
							.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
							.withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
							.withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, number)
							.withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE).build());
				}

				// ------------------------------------------------------ Home
				// Numbers
				/*
				  if (HomeNumber != null) {
				  ops.add(ContentProviderOperation.newInsert
				  (ContactsContract.Data.CONTENT_URI)
				  .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID,
				  0) .withValue(ContactsContract.Data.MIMETYPE,
				  ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
				  .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER,
				  HomeNumber)
				  .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
				  ContactsContract.CommonDataKinds.Phone.TYPE_HOME) .build());
				  }
				  
				  //------------------------------------------------------ Work
				  Numbers if (WorkNumber != null) {
				  ops.add(ContentProviderOperation
				  .newInsert(ContactsContract.Data.CONTENT_URI)
				  .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID,
				  0) .withValue(ContactsContract.Data.MIMETYPE,
				  ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
				  .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER,
				  WorkNumber)
				  .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
				  ContactsContract.CommonDataKinds.Phone.TYPE_WORK) .build());
				  }
				  
				  //------------------------------------------------------
				  Email if (emailID != null) {
				  ops.add(ContentProviderOperation.
				  newInsert(ContactsContract.Data.CONTENT_URI)
				  .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID,
				  0) .withValue(ContactsContract.Data.MIMETYPE,
				  ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
				  .withValue(ContactsContract.CommonDataKinds.Email.DATA,
				  emailID)
				  .withValue(ContactsContract.CommonDataKinds.Email.TYPE,
				  ContactsContract.CommonDataKinds.Email.TYPE_WORK) .build());
				  }
				  
				  //------------------------------------------------------
				  Organization if (!company.equals("") && !jobTitle.equals(""))
				  {
				  ops.add(ContentProviderOperation.newInsert(ContactsContract.
				  Data.CONTENT_URI)
				  .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID,
				  0) .withValue(ContactsContract.Data.MIMETYPE,
				  ContactsContract
				  .CommonDataKinds.Organization.CONTENT_ITEM_TYPE)
				  .withValue(ContactsContract
				  .CommonDataKinds.Organization.COMPANY, company)
				  .withValue(ContactsContract
				  .CommonDataKinds.Organization.TYPE,
				  ContactsContract.CommonDataKinds.Organization.TYPE_WORK)
				  .withValue
				  (ContactsContract.CommonDataKinds.Organization.TITLE,
				  jobTitle)
				  .withValue(ContactsContract.CommonDataKinds.Organization
				  .TYPE,
				  ContactsContract.CommonDataKinds.Organization.TYPE_WORK)
				  .build()); }
				 */
				// Asking the Contact provider to create a new contact
				try {
					context.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}
