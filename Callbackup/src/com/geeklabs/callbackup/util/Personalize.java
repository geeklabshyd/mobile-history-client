package com.geeklabs.callbackup.util;

public class Personalize {

	private boolean allPersonalizes;
	private boolean recievedCalls;
	private boolean dialedCalls;
	private boolean missedCalls;
	private boolean sms;
	private boolean contacts;
	private Long id;
	private Long userId;
	private boolean isWifi;
	private boolean isNwData;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isAllPersonalizes() {
		return allPersonalizes;
	}

	public void setAllPersonalizes(boolean allPersonalizes) {
		this.allPersonalizes = allPersonalizes;
	}

	public boolean isRecievedCalls() {
		return recievedCalls;
	}

	public void setRecievedCalls(boolean recievedCalls) {
		this.recievedCalls = recievedCalls;
	}

	public boolean isDialedCalls() {
		return dialedCalls;
	}

	public void setDialedCalls(boolean dialedCalls) {
		this.dialedCalls = dialedCalls;
	}

	public boolean isMissedCalls() {
		return missedCalls;
	}

	public void setMissedCalls(boolean missedCalls) {
		this.missedCalls = missedCalls;
	}

	public boolean isSms() {
		return sms;
	}

	public void setSms(boolean sms) {
		this.sms = sms;
	}
	
	public boolean isContacts() {
		return contacts;
	}
	
	public void setContacts(boolean contacts) {
		this.contacts = contacts;
	}

	public boolean isWifi() {
		return isWifi;
	}

	public void setWifi(boolean isWifi) {
		this.isWifi = isWifi;
	}

	public boolean isNwData() {
		return isNwData;
	}

	public void setNwData(boolean isNwData) {
		this.isNwData = isNwData;
	}
	
}
