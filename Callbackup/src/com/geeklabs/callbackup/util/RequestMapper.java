package com.geeklabs.callbackup.util;

public class RequestMapper {

	public final static String USER_LOGIN_SUCCESS = "/login/success";
	public final static String USER_LOGIN_ERROR = "/login/error";
	public static final String IS_ACTIVATED = "/activate";
	
	/**Admin**/
	public static final String ADMIN = "/admin";
	public static final String SETUP = "/admin/setup";
	
	/**AUTHENTICATION**/
	public static final String AUTHENTICATION = "/authentication";
	public static final String SIGNIN = "/signIn";
}
