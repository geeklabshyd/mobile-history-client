package com.geeklabs.callbackup.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;

import com.geeklabs.callbackup.SettingsActivity;
import com.geeklabs.callbackup.TabsActivity;
import com.geeklabs.callbackup.Preferences.AuthPreferences;

public class AlertDialogPopup {

	private static AuthPreferences authPreferences;
	public static <T> AlertDialog showAlertDialog(String title, String msg, String positiveMsg, String negativeMsg, final Context context,
			final Class<T> onPositveIntent, final Class<T> onNegativeIntent) {

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		alertDialogBuilder.setTitle(title);
		alertDialogBuilder.setMessage(msg);

		if (positiveMsg != null && !positiveMsg.isEmpty()) {
			alertDialogBuilder.setPositiveButton(positiveMsg, new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					if (onPositveIntent != null) {
						Intent i = new Intent(context, onPositveIntent);
						context.startActivity(i);
					}
				}
			});
		}

		if (negativeMsg != null && !negativeMsg.isEmpty()) {
			alertDialogBuilder.setNegativeButton(negativeMsg, new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					if (onNegativeIntent != null) {
						Intent i = new Intent(context, onNegativeIntent);
						context.startActivity(i);
					}
				}
			});
		}
		return alertDialogBuilder.create();
	}
	

	public static AlertDialog.Builder getAlertBuilder(String title, String msg, Context context) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		alertDialogBuilder.setTitle(title);
		alertDialogBuilder.setMessage(msg);
		return alertDialogBuilder;
	}

	public static void showWifiOrDataPreferencesAreEnabled(Activity activity) {
		AlertDialog showAlertDialog = AlertDialogPopup.showAlertDialog("Warning!", "Check your Settings Wifi or Data enabled.", "Ok", null,
				activity, SettingsActivity.class, null);
		showAlertDialog.show();
	}
	
	public static AlertDialog ShowNeutralPopup(String title, String msg, final Activity activity, final boolean isAuthFlow) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
		alertDialogBuilder.setTitle(title);
		alertDialogBuilder.setMessage(msg);
		alertDialogBuilder.setNeutralButton("Ok", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (isAuthFlow) {
					triggerTabsActivity(activity);
				}
			}
		});
		return alertDialogBuilder.create();
	}
	
	private final static void triggerTabsActivity(Activity context) {
		// Show home tabs activity
		Intent intent = new Intent(context, TabsActivity.class);
		context.startActivity(intent);
		context.finish();
	}
}
