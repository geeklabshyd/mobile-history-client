package com.geeklabs.callbackup.util;

import com.geeklabs.callbackup.SyncService;
import com.geeklabs.callbackup.TabsActivity;
import com.geeklabs.callbackup.communication.task.post.SyncContactsTask;
import com.geeklabs.callbackup.domain.SyncPreferences;
import com.geeklabs.callbackup.sync.devicetoapp.task.SyncDeviceToAppCallsTask;
import com.geeklabs.callbackup.sync.devicetoapp.task.SyncDeviceToAppSmsesTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;

public class SyncUtil implements SyncService {

	private SyncPreferences syncPreferences;
	String itemsToList[] = new String[4];
	private ProgressDialog progressDialog;

	public void syncNow(final Activity context, final boolean isAuthFlow) {
		itemsToList[0] = Constants.SYNC_CALLS;
		itemsToList[1] = Constants.SYNC_SMS;
		itemsToList[2] = Constants.SYNC_CONTACTS;
		itemsToList[3] = Constants.SYNC_ALL;

		syncPreferences = new SyncPreferences();

		final Builder alertBuilder = AlertDialogPopup.getAlertBuilder("Sync Now", null, context);
		alertBuilder.setMultiChoiceItems(itemsToList, new boolean[] { false, false, false, true },
				new OnMultiChoiceClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which, boolean isChecked) {
						String item = itemsToList[which];
						// Add status to sync preferences
						setStatusToItem(item, isChecked);
					}
				}).setPositiveButton("Sync", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// on click on Sync now based on sync preferences we
						// have to perform
						if (NetworkService.isNetWorkAvailable(context)) {
							performSyncNow(context, isAuthFlow);
						} else {
							showNetworkUnAvailablePopup(context);
						}
					}
				}).setNegativeButton("Cancel", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (isAuthFlow) {
							triggerTabsActivity(context);
						}
					}
				});
		alertBuilder.create();
		alertBuilder.show();
	}

	private final void triggerTabsActivity(Activity context) {
		// Show home tabs activity
		Intent intent = new Intent(context, TabsActivity.class);
		context.startActivity(intent);
		context.finish();
	}

	public void setStatusToItem(String item, boolean isChecked) {
		if (item.equals(Constants.SYNC_SMS)) {
			syncPreferences.setSyncSms(isChecked);
		} else if (item.equals(Constants.SYNC_CALLS)) {
			syncPreferences.setSyncCall(isChecked);
		} else if (item.equals(Constants.SYNC_ALL)) {
			syncPreferences.setSyncAll(isChecked);
		} else if (item.equals(Constants.SYNC_CONTACTS)) {
			syncPreferences.setSyncContacts(isChecked);
		}
	}

	private void performSyncNow(Activity activity, boolean isAuthFlow) {
		doSyncIfWifiOrDataEnable(activity, isAuthFlow);
	}

	private void doSyncIfWifiOrDataEnable(Activity activity, boolean isAuthFlow) {
		if (NetworkService.isSyncWifiOrDataEnabled(activity.getApplicationContext())) {
			syncDeviceAndApp(activity, isAuthFlow);
			// store the data in sqlite and open activity

		} else {
			// check preferences
			AlertDialogPopup.showWifiOrDataPreferencesAreEnabled(activity);
		}
	}

	private void syncDeviceAndApp(Activity activity, boolean isAuthFlow) {
		// here show just Dialog
		// AlertDialog showNeutralPopup =
		// AlertDialogPopup.ShowNeutralPopup("Sync Info", "Depending on backup
		// data in your mobile, overall Sync process might take some time!",
		// activity, isAuthFlow);
		// showNeutralPopup.show();

		// If nothing is selected for sync don't start spinner
		/*
		 * if (!syncPreferences.isSyncAll() && !syncPreferences.isSyncCall() &&
		 * !syncPreferences.isSyncSms() && !syncPreferences.isSyncContacts()) {
		 * // Start Spinner to show sync in progress
		 * activity.setProgressBarIndeterminateVisibility(true); }
		 */

		// sync device calls as app calls

		if (syncPreferences.isSyncAll() || syncPreferences.isSyncCall()) {
			SyncDeviceToAppCallsTask syncDeviceToAppCallsTask = new SyncDeviceToAppCallsTask(syncPreferences, activity,
					this);
			syncDeviceToAppCallsTask.execute();
		}

		// sync device smses as app smses

		if (syncPreferences.isSyncAll() || syncPreferences.isSyncSms()) {
			SyncDeviceToAppSmsesTask syncDeviceToAppSmsesTask = new SyncDeviceToAppSmsesTask(syncPreferences, activity,
					this);
			syncDeviceToAppSmsesTask.execute();
		}

		// sync device contacts as app contacts
		if (syncPreferences.isSyncAll() || syncPreferences.isSyncContacts()) {
//			syncStarted(activity); not requied
			SyncContactsTask contactsTask = new SyncContactsTask(activity, syncPreferences);
			contactsTask.execute();

			if (!syncPreferences.isSyncAll()) {
				AlertDialog showNeutralPopup = AlertDialogPopup.ShowNeutralPopup("Contacts Sync",
						"Your contacts will be sync on background!", activity, isAuthFlow);
				showNeutralPopup.show();
			}
		}

	}

	private void showNetworkUnAvailablePopup(final Activity context) {
		AlertDialog showAlertDialog = AlertDialogPopup.showAlertDialog("Network warning!",
				"Check your network connection and try later.", "Ok", null, context, TabsActivity.class, null);
		showAlertDialog.show();
	}

	@Override
	public void syncStarted(Activity activity) {
		/*progressDialog = new ProgressDialog(activity);
		progressDialog.setMessage("Sync is in progress please wait..");
		progressDialog.setCancelable(false);
		progressDialog.show();*/

	}

	@Override
	public void syncCompleted() {
		/*if (progressDialog != null) {
			progressDialog.cancel();
		}*/
	}
}