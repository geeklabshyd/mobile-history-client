package com.geeklabs.callbackup.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.geeklabs.callbackup.Preferences.AuthPreferences;
import com.geeklabs.callbackup.domain.SMS;

public class DeviceSms {
	private static final String INBOX_URI = "content://sms/inbox";
	private static final String SENTBOX_URI = "content://sms/sent";

	public static List<SMS> getAllInboxSmsesFromDevice(Context context) {
		AuthPreferences authPreferences = new AuthPreferences(context);
		ContentResolver contentResolver = context.getContentResolver();
		Cursor cursor = contentResolver.query(Uri.parse(INBOX_URI), null, null, null, null);

		List<SMS> smses = new ArrayList<SMS>();
		cursor.moveToFirst();
		while (cursor.isAfterLast() == false) {
			int index_Address = cursor.getColumnIndex("address"); // number
			int index_Body = cursor.getColumnIndex("body"); // message
			int index_Date = cursor.getColumnIndex("date"); // date

			String number = cursor.getString(index_Address);
			String message = cursor.getString(index_Body);

			long longDate = cursor.getLong(index_Date);
			SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_SAVE_FORMAT);
			String formatedDate = format.format(longDate);

			SMS sms = new SMS();
			// get contact Name based on Number
			String contactName = Contacts.getContactName(context, number);
			sms.setContactName(contactName);
			sms.setDate(formatedDate);
			sms.setFrom(number);
			sms.setMessage(message);
			sms.setUserId(authPreferences.getUserId());
			sms.setType(Constants.SMS_RECEIVED);

			smses.add(sms);
			cursor.moveToNext();
		}
		cursor.close();
		
		return smses;
	}

	public static List<SMS> getAllSentSmsesFromDevice(Context context) {
		AuthPreferences authPreferences = new AuthPreferences(context);
		ContentResolver contentResolver = context.getContentResolver();
		Cursor cursor = contentResolver.query(Uri.parse(SENTBOX_URI), null, null, null, null);

		List<SMS> smses = new ArrayList<SMS>();

		cursor.moveToFirst();
		while (cursor.isAfterLast() == false) {
			int index_Address = cursor.getColumnIndex("address"); // number
			int index_Body = cursor.getColumnIndex("body"); // message
			int index_Date = cursor.getColumnIndex("date"); // date

			String number = cursor.getString(index_Address);
			String message = cursor.getString(index_Body);
			long longDate = cursor.getLong(index_Date);

			SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_SAVE_FORMAT);
			String formatedDate = format.format(longDate);

			SMS sms = new SMS();
			// get contact Name based on Number
			String contactName = Contacts.getContactName(context, number);
			sms.setContactName(contactName);
			sms.setDate(formatedDate);
			sms.setFrom(number);
			sms.setMessage(message);
			sms.setUserId(authPreferences.getUserId());
			sms.setType(Constants.SMS_DELIVERED);

			smses.add(sms);

			cursor.moveToNext();
		}
		cursor.close();
		return smses;
	}
}
