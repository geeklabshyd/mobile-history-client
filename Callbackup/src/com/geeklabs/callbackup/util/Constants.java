package com.geeklabs.callbackup.util;

public final class Constants {

	private Constants(){
	}
	
	//for sync all types
	public static final String SYNC_CALLS = "Sync Calls";
	public static final String SYNC_ONLY_ON_WI_FI_AVAILABLE = "Sync only on Wi-Fi available";
	public static final String SYNC_ALL = "Sync All";
	public static final String SYNC_CONTACTS = "Sync Contacts";
	public static final String SYNC_DIALED_CALLS = "Sync Dialed calls";
	public static final String SYNC_RECEIVED_CALLS = "Sync Received calls";
	public static final String SYNC_MISSED_CALLS = "Sync Missed calls";
	public static final String SYNC_SMS = "Sync SMS";
	public static final String SYNC_ON_WI_FI_OR_MOBILE_DATA_AVAILABLE = "Sync on Wi-Fi or Mobile Data available";
	
	
	//for sync all 
	public static final int SYNC_NOW_REQ_CODE = 2014;
	
	//for date format "dd/MM/yyyy HH:mm:ss"
	public static final String DATE_SAVE_FORMAT = "dd/MM/yyyy HH:mm:ss";
	
	// SMS types
	public static final int SMS_RECEIVED = 1;
	public static final int SMS_DELIVERED = 2;
	
	//for call types
	public static final int INCOMING = 1;
	public static final int OUTGOING = 2;
	public static final int MISSED = 3;
	
	
	
	//for old calls 
	public static final String CALL_LOG_URI = "content://call_log/calls"; //CallLog.Calls.CONTENT_URI think about it
	
	//for phone book
	public static final int ADD_CONTACT = 555;
}
