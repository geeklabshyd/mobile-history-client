package com.geeklabs.callbackup.util;

public class RequestUrl {

	// Balu 192.168.0.16
	/*public static final String VALIDATE_TOKEN = "http://192.168.0.16:8899/authentication/signIn";
	public static final String SEND_SMS_HISTORY_REQ = "http://192.168.0.16:8899/sms/saveSms/{id}";
	public static final String SEND_CALL_HISTORY_REQ = "http://192.168.0.16:8899/call/saveCallHistory/{id}";
	public static final String SIGNOUT = "http://192.168.0.16:8899/authentication/signout/{id}";
	public static final String PERSONLIZE_REQ = "http://192.168.0.16:8899/personalize/save/{id}";
	public static final String GET_MISSED_CALL_HISTORY = "http://192.168.0.16:8899/call/getMissedCalls/{id}";
	public static final String GET_DIALED_CALL_HISTORY = "http://192.168.0.16:8899/call/getDialedCalls/{id}";
	public static final String GET_RECEIVED_CALL_HISTORY = "http://192.168.0.16:8899/call/getReceviedCalls/{id}";
	public static final String GET_SMS_HISTORY = "http://192.168.0.16:8899/sms/getSmsHistory/{id}";
	public static final String SEND_PHONE_BOOK = "http://192.168.0.16:8899/phonebook/savePhoneBook/{id}";
	public static final String GET_ALL_CALLS = "http://192.168.0.16:8899/call/getAllCalls/{id}"; 
	public static final String GET_ALL_SMS = "http://192.168.0.16:8899/sms/getAllSmses/{id}";
	public static final String SYNC_NOW = "http://192.168.0.16:8899/sync/syncNow/{id}";
	public static final String GET_CALLS_BY_PAGABLE = "http://192.168.0.16:8899/call/getCallsByPage/{id}";
	public static final String GET_SERVER_DELETED_RECORDS = "http://192.168.0.16:8899/delete/getDeletedRecords/{id}";
	*/
	
//	Actual URLs
	public static final String VALIDATE_TOKEN = "https://smsandcallhistory.appspot.com/authentication/signIn";
	public static final String SEND_SMS_HISTORY_REQ = "https://smsandcallhistory.appspot.com/sms/saveSms/{id}";
	public static final String SEND_CALL_HISTORY_REQ = "https://smsandcallhistory.appspot.com/call/saveCallHistory/{id}";
	public static final String SIGNOUT = "https://smsandcallhistory.appspot.com/authentication/signout/{id}";
	public static final String PERSONLIZE_REQ = "https://smsandcallhistory.appspot.com/personalize/save/{id}";
	public static final String GET_MISSED_CALL_HISTORY = "https://smsandcallhistory.appspot.com/call/getMissedCalls/{id}";
	public static final String GET_DIALED_CALL_HISTORY = "https://smsandcallhistory.appspot.com/call/getDialedCalls/{id}";
	public static final String GET_RECEIVED_CALL_HISTORY = "https://smsandcallhistory.appspot.com/call/getReceviedCalls/{id}";
	public static final String GET_SMS_HISTORY = "https://smsandcallhistory.appspot.com/sms/getSmsHistory/{id}";
	public static final String SEND_PHONE_BOOK = "https://smsandcallhistory.appspot.com/phonebook/savePhoneBook/{id}";
	public static final String GET_ALL_CALLS = "https://smsandcallhistory.appspot.com/call/getAllCalls/{id}"; 
	public static final String GET_ALL_SMS = "https://smsandcallhistory.appspot.com/sms/getAllSmses/{id}";
	public static final String SYNC_NOW = "https://smsandcallhistory.appspot.com/sync/syncNow/{id}";
	public static final String GET_CALLS_BY_PAGABLE = "https://smsandcallhistory.appspot.com/call/getCallsByPage/{id}";
	public static final String GET_SERVER_DELETED_RECORDS = "https://smsandcallhistory.appspot.com/delete/getDeletedRecords/{id}";

	

}
