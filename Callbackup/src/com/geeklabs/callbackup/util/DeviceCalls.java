package com.geeklabs.callbackup.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;

import com.geeklabs.callbackup.Preferences.AuthPreferences;
import com.geeklabs.callbackup.domain.CallHistory;

public class DeviceCalls {
	
	public static List<CallHistory> getAllCallLogs(Context context) {
		ContentResolver contentResolver = context.getContentResolver();
		Cursor cursor = contentResolver.query(Uri.parse(Constants.CALL_LOG_URI), null, null, null, null);
		AuthPreferences authPreferences = new AuthPreferences(context);
		
		List<CallHistory> callHistories = new ArrayList<CallHistory>();
		while (cursor.moveToNext()) {
			int number = cursor.getColumnIndex(CallLog.Calls.NUMBER);
			int type = cursor.getColumnIndex(CallLog.Calls.TYPE);
			int date = cursor.getColumnIndex(CallLog.Calls.DATE);
//			int duration = cursor.getColumnIndex(CallLog.Calls.DURATION);
			
			String contactNumber = cursor.getString(number);
			long dateLong = cursor.getLong(date);
			int callType = cursor.getInt(type);
//			String callDuration = cursor.getString(duration);
			
			//create a call object
			CallHistory callHistory = new CallHistory();
			callHistory.setCallType(callType); // Incoming, missed or Outgoing ?
			
			
			//get Contactname by Number
			String contactName = Contacts.getContactName(context, contactNumber);
			callHistory.setContactName(contactName);
			
			SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_SAVE_FORMAT);
			String formatedDate = format.format(dateLong);
			callHistory.setDate(formatedDate);
			
			callHistory.setNumber(contactNumber);
			try {
				callHistory.setTime(String.valueOf(format.parse(formatedDate).getTime()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			callHistory.setUserId(authPreferences.getUserId());
			
			//add to collection 
			callHistories.add(callHistory);
		}
		cursor.close();
		return callHistories;
	}
}
