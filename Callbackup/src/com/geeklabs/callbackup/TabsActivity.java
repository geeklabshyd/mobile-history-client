package com.geeklabs.callbackup;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TabWidget;

import com.geeklabs.callbackup.Preferences.AuthPreferences;
import com.geeklabs.callbackup.listfragment.tab.DialledCallsFragment;
import com.geeklabs.callbackup.listfragment.tab.MissedCallsFragment;
import com.geeklabs.callbackup.listfragment.tab.ReceivedCallsFragment;
import com.geeklabs.callbackup.listfragment.tab.SmsInfoFragment;
import com.geeklabs.callbackup.util.Constants;
import com.geeklabs.callbackup.util.SyncUtil;
import com.google.android.gms.ads.AdView;

public class TabsActivity extends FragmentActivity {
	// private AuthPreferences authPreferences;
	private TabHost mTabHost;
	private ViewPager mViewPager;
	private static TabsAdapter mTabsAdapter;
	private AuthPreferences authPreferences;

	private static final int SYNC_NOW_REQ_CODE = 2014;
	private AdView adView;

	private void getOverflowMenu() {
		try {
			ViewConfiguration config = ViewConfiguration.get(this);
			Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
			if (menuKeyField != null) {
				menuKeyField.setAccessible(true);
				menuKeyField.setBoolean(config, false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

		// authPreferences = new AuthPreferences(getApplicationContext());
		setContentView(R.layout.tab_activity_main);

		authPreferences = new AuthPreferences(getApplicationContext());
		// Action bar back ground color
		/*BitmapDrawable bitmap = (BitmapDrawable) getResources().getDrawable(R.drawable.steel_blue);
		bitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
		getActionBar().setBackgroundDrawable(bitmap);*/

		mTabHost = (TabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup();
		mViewPager = (ViewPager) findViewById(R.id.pager);
		//mViewPager.setBackgroundColor(Color.BLACK);
		mTabsAdapter = new TabsAdapter(this, mTabHost, mViewPager);

		// Dialled call list
		TabSpec dialledTab = mTabHost.newTabSpec("DialledCallList").setIndicator("", getResources().getDrawable(R.drawable.sym_call_outgoing));
		mTabsAdapter.addTab(dialledTab, DialledCallsFragment.class, null);

		// Missed call list
		TabSpec missedTab = mTabHost.newTabSpec("MissedCallList").setIndicator("", getResources().getDrawable(R.drawable.sym_call_missed));
		mTabsAdapter.addTab(missedTab, MissedCallsFragment.class, null);

		// Received call list
		TabSpec receivedTab = mTabHost.newTabSpec("ReceivedCallList").setIndicator("", getResources().getDrawable(R.drawable.sym_call_incoming));
		mTabsAdapter.addTab(receivedTab, ReceivedCallsFragment.class, null);

		// SMS Info
		TabSpec smstab = mTabHost.newTabSpec("SmsInfo").setIndicator("", getResources().getDrawable(R.drawable.sym_action_email));
		mTabsAdapter.addTab(smstab, SmsInfoFragment.class, null);
//		setTabColor();

		getOverflowMenu();

		// Adds code
		// http://www.androidbegin.com/tutorial/integrating-new-google-admob-banner-interstitial-ads/
		adView = (AdView) this.findViewById(R.id.adView);

		adView.loadAd(AdRequestUtil.getAdRequest());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.itemlist, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.myAccount:
			Intent myAcc = new Intent(getApplicationContext(), ProfileActivity.class);
			startActivity(myAcc);
			break;
		case R.id.settings:
			Intent i = new Intent(getApplicationContext(), SettingsActivity.class);
			startActivity(i);
			break;
		case R.id.syncnow:
			showSyncNowPopup();
			break;
		case R.id.access_server:
			Intent i1 = new Intent(getApplicationContext(), HelpActivity.class);
			startActivity(i1);
			break;
		case android.R.id.home:
			break;

		}
		return super.onOptionsItemSelected(item);
	}

	private void showSyncNowPopup() {
		SyncUtil syncUtil = new SyncUtil();
		syncUtil.syncNow(this, false);
	}

	@Override
	public void onBackPressed() {
		moveTaskToBack(true);
	}

	// inner class started
	public static class TabsAdapter extends FragmentPagerAdapter implements TabHost.OnTabChangeListener, ViewPager.OnPageChangeListener {
		private final Context mContext;
		private final TabHost mTabHost;
		private final ViewPager mViewPager;
		private final ArrayList<TabInfo> mTabs = new ArrayList<TabInfo>();
		private final HashMap<Integer, Fragment> tabActivities = new HashMap<Integer, Fragment>();

		static final class TabInfo {
			private final Class<?> clss;
			private final Bundle args;

			TabInfo(String _tag, Class<?> _class, Bundle _args) {
				clss = _class;
				args = _args;
			}
		}

		static class DummyTabFactory implements TabHost.TabContentFactory {
			private final Context mContext;

			public DummyTabFactory(Context context) {
				mContext = context;
			}

			@Override
			public View createTabContent(String tag) {
				View v = new View(mContext);
				v.setMinimumWidth(0);
				v.setMinimumHeight(0);
				return v;
			}
		}

		public TabsAdapter(FragmentActivity activity, TabHost tabHost, ViewPager pager) {
			super(activity.getSupportFragmentManager());
			mContext = activity;
			mTabHost = tabHost;
			mViewPager = pager;
			mTabHost.setOnTabChangedListener(this);

			mViewPager.setAdapter(this);
			mViewPager.setOnPageChangeListener(this);
			mViewPager.setOffscreenPageLimit(4);
		}

		public void addTab(TabHost.TabSpec tabSpec, Class<?> clss, Bundle args) {
			tabSpec.setContent(new DummyTabFactory(mContext));
			String tag = tabSpec.getTag();

			TabInfo info = new TabInfo(tag, clss, args);
			mTabs.add(info);
			mTabHost.addTab(tabSpec);

			notifyDataSetChanged();
		}

		@Override
		public void onPageScrollStateChanged(int arg0) {
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
		}

		@Override
		public void onPageSelected(int position) {
			TabWidget widget = mTabHost.getTabWidget();
			int oldFocusability = widget.getDescendantFocusability();
			widget.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
			mTabHost.setCurrentTab(position);

			widget.setDescendantFocusability(oldFocusability);
		}

		@Override
		public void onTabChanged(String tabId) {
			int position = mTabHost.getCurrentTab();
			mViewPager.setBackgroundColor(Color.WHITE);

			// setTabColor(mTabHost);
			mViewPager.setCurrentItem(position);

			Fragment fragment = tabActivities.get(position);
			if (fragment != null) {
				fragment.onResume();
			}
		}

		@Override
		public int getCount() {
			return mTabs.size();
		}

		@Override
		public Fragment getItem(int position) {
			TabInfo info = mTabs.get(position);

			Fragment fragment = tabActivities.get(position);
			if (fragment == null) {
				fragment = Fragment.instantiate(mContext, info.clss.getName(), info.args);
				tabActivities.put(position, fragment);
			}

			return fragment;
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (!authPreferences.isUserSignedIn() || !(authPreferences.getUserId() > 0)) {
			Intent i = new Intent(this, MainActivity.class);
			startActivity(i);
			finish();
		}
		if (adView != null) {
			adView.resume();
		}
	}

	// on activity results
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_OK) {
			if (requestCode == SYNC_NOW_REQ_CODE) {
				mTabsAdapter.notifyDataSetChanged();
			}
		}
	}

	public static TabsAdapter getMTabsAdaperToNofify() {
		return mTabsAdapter;
	}

	/** Called when leaving the activity */
	@Override
	public void onPause() {
		if (adView != null) {
			adView.pause();
		}
		super.onPause();
	}

	/** Called before the activity is destroyed */
	@Override
	public void onDestroy() {
		if (adView != null) {
			adView.destroy();
		}
		super.onDestroy();
	}

	/*public void setTabColor() {
		int totalTabs = mTabHost.getTabWidget().getChildCount();
		for (int i = 0; i < totalTabs; i++) {
			if (mTabHost.getTabWidget().getChildAt(i).isSelected()) {
				mTabHost.getTabWidget().getChildAt(i).setBackgroundResource(R.drawable.bg_four); // selector
																									// xml
																									// for
																									// selected
				mTabHost.getTabWidget().setStripEnabled(false);
				mTabHost.getTabWidget().setRightStripDrawable(R.drawable.bg_five);
				mTabHost.getTabWidget().setLeftStripDrawable(R.drawable.bg_one);
			}
		}
	}*/
}
