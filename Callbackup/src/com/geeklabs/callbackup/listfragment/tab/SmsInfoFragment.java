package com.geeklabs.callbackup.listfragment.tab;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Intents;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.ListFragment;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnActionExpandListener;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.geeklabs.callbackup.R;
import com.geeklabs.callbackup.TabsActivity;
import com.geeklabs.callbackup.adapter.SmsAdapter;
import com.geeklabs.callbackup.communication.task.get.GetOfflineSMSBaseTask;
import com.geeklabs.callbackup.communication.task.post.SendSmsTask;
import com.geeklabs.callbackup.domain.SMS;
import com.geeklabs.callbackup.pagenation.EndlessScrollListener;
import com.geeklabs.callbackup.util.Constants;
import com.geeklabs.callbackup.util.NetworkService;

public class SmsInfoFragment extends ListFragment implements SearchView.OnQueryTextListener, SearchView.OnCloseListener {
	private SmsAdapter smsAdapter;
	private String searchTerm;
	private EndlessScrollListener endlessScrollListener;
	private SearchView searchView;
	private final int CONTEXT_MENU_ADD_CONTACT = 10;
	private final int CONTEXT_MENU_CALL = 11;
	private final int CONTEXT_MENU_SEND_SMS = 12;
	private final int CONTEXT_MENU_FORWARD_SMS = 13;
	private final int CONTEXT_MENU_SYNC_SMS = 14;

	@Override
	public void onResume() {
		super.onResume();
		if (smsAdapter != null && smsAdapter.getsms().isEmpty()) {
			getSmses(searchTerm);
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);

		searchView = (SearchView) menu.findItem(R.id.search).getActionView();
		setupSearchView(searchView);
	}

	private void setupSearchView(SearchView searchView) {
		searchView.setQuery("", false);
		searchView.setIconified(true);
		searchView.setOnQueryTextListener(this);
		searchView.setOnCloseListener(this);
		searchView.clearFocus();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);
		smsAdapter = new SmsAdapter(getActivity());
		setListAdapter(smsAdapter);

		// Register for endless scroll bar
		registerForPagination();
		registerForContextMenu(getListView());
		//OnLongClickOnItem(getListView());

		/*
		 * // On list item clicked getListView().setOnItemClickListener(new
		 * OnItemClickListener() {
		 * 
		 * @Override public void onItemClick(AdapterView<?> parent, View view,
		 * int position, long id) { SMS sms = smsAdapter.getsms().get(position);
		 * Toast.makeText(getActivity().getApplicationContext(),
		 * String.valueOf(sms.getServerSmsId()), Toast.LENGTH_LONG).show(); }
		 * });
		 */
	}

	/*private void OnLongClickOnItem(ListView listView) {
		ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(getActivity().getApplicationContext().CLIPBOARD_SERVICE);
		ClipData clip = ClipData.newPlainText("label", "copied text");
		clipboard.setPrimaryClip(clip);

	}*/

	public void getSmses(final String searchTerm) {
		final FragmentActivity activity = getActivity();
		getOfflineSmses(activity, 0, searchTerm);
		
		
		/*
		
		// get tracks in background async task
		GetOfflineSMSBaseTask getSms = new GetOfflineSMSBaseTask(activity, 0, searchTerm) {
			@Override
			protected void updateUI(final List<SMS> smses) {
				activity.runOnUiThread(new Runnable() {
					public void run() {
						// Notify adapter with new track list
						if (smsAdapter != null && smses != null && !smses.isEmpty()) {
							List<SMS> smsesList = new ArrayList<SMS>();
							smsesList.addAll(smses);

							smsAdapter.setSMS(smsesList);
							smsAdapter.notifyDataSetChanged();
						}
					}
				});
			}

			@Override
			protected void stopProgressBar() {
				showEmptyText("Smses not found...");
			}
		};
		getSms.execute();
	*/}

	// show empty text
	private void showEmptyText(String msg) {
		View root = getView();
		if (root != null) {
			SmsInfoFragment.this.setEmptyText(msg);
		}
	}

	public void registerForPagination() {
		final FragmentActivity activity = getActivity();
		// Listen for scroll change, Are we at the end of list view ?
		endlessScrollListener = new EndlessScrollListener(0) {
			@Override
			protected void runTask(int currentPage) {
				getOfflineSmses(activity, currentPage, searchTerm);
			}
		};
		endlessScrollListener.setCurrentPage(0);
		endlessScrollListener.setSearchQuery(searchTerm);
		SmsInfoFragment.this.getListView().setOnScrollListener(endlessScrollListener);
	}
	
	
	private void getOfflineSmses(final FragmentActivity activity, int currentPage, String searchTerm) {
		// get calls in background async task
		GetOfflineSMSBaseTask smsBaseTask = new GetOfflineSMSBaseTask(activity, currentPage, searchTerm) {
			@Override
			protected void updateUI(final List<SMS> smses) {
				activity.runOnUiThread(new Runnable() {
					public void run() {
						// Notify adapter with new track list
						if (smsAdapter != null && smses != null && !smses.isEmpty()) {

							List<SMS> smsList = new ArrayList<SMS>();
							smsList.addAll(smses);

							smsAdapter.setSMS(smsList);
							smsAdapter.notifyDataSetChanged();
						}
					}
				});
			}

			@Override
			protected void stopProgressBar() {
				showEmptyText("Smses not found...");
			}

		};
		smsBaseTask.execute();
	}

	@Override
	public boolean onClose() {
		if (searchView != null) {
			searchView.setIconified(true);
		}

		this.searchTerm = null;

		resetPagination(this.searchTerm);
		return false;
	}

	private void resetPagination(String searchQuery) {
		// Reset Tab context info
		smsAdapter.emptyList();
		smsAdapter.notifyDataSetChanged();
		endlessScrollListener.setCurrentPage(0);
		endlessScrollListener.setSearchQuery(searchQuery);

		// Get search result
		getSmses(searchTerm);
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		this.searchTerm = query;
		resetPagination(this.searchTerm);

		return true;
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		if (newText == null || newText.isEmpty()) {
			this.searchTerm = null;
			resetPagination(this.searchTerm);
		}
		return false;
	}

	@SuppressLint("NewApi")
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.search:
			item.setOnActionExpandListener(new OnActionExpandListener() {

				@Override
				public boolean onMenuItemActionExpand(MenuItem item) {

					return true;
				}

				@Override
				public boolean onMenuItemActionCollapse(MenuItem item) {
					resetPagination(null);
					return true;
				}
			});
		default:
			break;
		}

		return super.onOptionsItemSelected(item);

	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		// Context menu
		menu.setHeaderTitle("More Options");
		menu.add(Menu.NONE, CONTEXT_MENU_ADD_CONTACT, Menu.NONE, "Add/Update contact");
		menu.add(Menu.NONE, CONTEXT_MENU_CALL, Menu.NONE, "Call");
		menu.add(Menu.NONE, CONTEXT_MENU_SEND_SMS, Menu.NONE, "Reply");
		menu.add(Menu.NONE, CONTEXT_MENU_FORWARD_SMS, Menu.NONE, "Forward");
		menu.add(Menu.NONE, CONTEXT_MENU_SYNC_SMS, Menu.NONE, "Sync SMS");

	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		SMS sms = new SMS();
		FragmentActivity activity = getActivity();
		if (activity instanceof TabsActivity) {
			ContextMenuInfo menuInfo = item.getMenuInfo();
			AdapterContextMenuInfo adapterContextMenuInfo = (AdapterContextMenuInfo) menuInfo;
			int position = adapterContextMenuInfo.position;
			if (position < smsAdapter.getsms().size()) {
				sms = (SMS) smsAdapter.getItem(position);
			}

			if (getUserVisibleHint()) {
				switch (item.getItemId()) {
				case CONTEXT_MENU_ADD_CONTACT:
					String number = sms.getFrom();

					if (number != null) {
						Intent contactIntent = new Intent(Intents.Insert.ACTION);
						contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
						contactIntent.putExtra(Intents.Insert.PHONE, number);
						startActivityForResult(contactIntent, Constants.ADD_CONTACT);
					}

					return true;
				case CONTEXT_MENU_CALL:
					// call action
					String numberDail = sms.getFrom();
					if (numberDail != null) {
						Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + numberDail));
						startActivity(callIntent);
					}

					return true;

				case CONTEXT_MENU_SEND_SMS:
					String sentSmsNumber = sms.getFrom();
					if (sentSmsNumber != null) {
						Intent sendIntent = new Intent(Intent.ACTION_SENDTO);
						sendIntent.setData(Uri.parse("sms:" + sentSmsNumber));
						startActivity(sendIntent);
					}

					return true;

				case CONTEXT_MENU_FORWARD_SMS:
					// String forwardSmsNumber = sms.getFrom();
					Intent forwardIntent = new Intent(Intent.ACTION_VIEW);
					String message = sms.getMessage();
					if (message != null) {
						forwardIntent.putExtra("sms_body", message);
						forwardIntent.setType("vnd.android-dir/mms-sms");
						startActivity(forwardIntent);
					}
					return true;

				case CONTEXT_MENU_SYNC_SMS:
					// sync sms

					if (!sms.isSyncked() && sms.getServerSmsId() == null) {
						syncSMS(sms);
					} else {
						Toast.makeText(getActivity(), "SMS already Synced!", Toast.LENGTH_LONG).show();
					}
					return true;
				}
			}
		}
		return false;

	}

	private void syncSMS(SMS sms) {
		if (NetworkService.isNetWorkAvailable(getActivity())) {
			SendSmsTask sendSmsTask = new SendSmsTask(null, getActivity(), sms);
			sendSmsTask.execute();
		} else {
			Toast.makeText(getActivity(), "check your network conection", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == Constants.ADD_CONTACT) {
				Toast.makeText(getActivity(), "contact Added", Toast.LENGTH_LONG).show();
			}
		} else {
			Toast.makeText(getActivity(), "contact Not Added", Toast.LENGTH_LONG).show();
		}
	}
}
