package com.geeklabs.callbackup.listfragment.tab;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Intents;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnActionExpandListener;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.SearchView;

import com.geeklabs.callbackup.CustomSwipeRefreshLayout;
import com.geeklabs.callbackup.R;
import com.geeklabs.callbackup.TabsActivity;
import com.geeklabs.callbackup.adapter.CallsAdapter;
import com.geeklabs.callbackup.communication.task.get.GetOfflineCallsByStatusBaseTask;
import com.geeklabs.callbackup.domain.CallHistory;
import com.geeklabs.callbackup.pagenation.EndlessScrollListener;
import com.geeklabs.callbackup.util.Constants;

public abstract class CallsTab extends ListFragment implements SwipeRefreshLayout.OnRefreshListener, SearchView.OnQueryTextListener,
		SearchView.OnCloseListener{
	private CallsAdapter callsAdapter;
	private String searchQuery = "";
	private EndlessScrollListener endlessScrollListener;
	private SearchView searchView;
	private CustomSwipeRefreshLayout swipeRefreshLayout;

	final int CONTEXT_MENU_ADD_CONTACT = 1;
	final int CONTEXT_MENU_CALL = 2;
	final int CONTEXT_MENU_SEND_SMS = 3;

	@Override
	public void onResume() {
		super.onResume();
		if (callsAdapter != null && callsAdapter.getCallHistories().isEmpty()) {
			getCalls(searchQuery);
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);

		searchView = (SearchView) menu.findItem(R.id.search).getActionView();
		setupSearchView(searchView);
	}

	@SuppressLint("NewApi")
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.search:
			item.setOnActionExpandListener(new OnActionExpandListener() {

				@Override
				public boolean onMenuItemActionExpand(MenuItem item) {

					return true;
				}

				@Override
				public boolean onMenuItemActionCollapse(MenuItem item) {
					resetPagination(null);
					return true;
				}
			});
		default:
			break;
		}

		return super.onOptionsItemSelected(item);

	}

	private void setupSearchView(SearchView searchView) {
		searchView.setQuery("", false);
		searchView.setIconified(true);
		searchView.setOnQueryTextListener(this);
		searchView.setOnCloseListener(this);
		searchView.clearFocus();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);

		

		callsAdapter = new CallsAdapter(getActivity());
		setListAdapter(callsAdapter);

		// Register for endless scroll bar
		registerForPagination();
		registerForContextMenu(getListView());

	}

	protected void registerForPagination() {
		// Listen for scroll change, Are we at the end of list view ?
		endlessScrollListener = new EndlessScrollListener(0) {

			@Override
			protected void runTask(int currentPage) {
				getOfflineCalls(currentPage, getSearchQuery());
			}
		};

		endlessScrollListener.setCurrentPage(0);
		endlessScrollListener.setSearchQuery(searchQuery);
		this.getListView().setOnScrollListener(endlessScrollListener);
	}
	
	private void getOfflineCalls(int currentPage, String searchQuery) {
		final FragmentActivity activity = getActivity();
		// get calls in background async task
		ProgressDialog progressDialog = new ProgressDialog(activity);
		progressDialog.setMessage("Sync is in progress...");
		progressDialog.setCancelable(true);
		GetOfflineCallsByStatusBaseTask getOfflineCallsByStatusBaseTask = new GetOfflineCallsByStatusBaseTask(activity, progressDialog, currentPage,
				getCallType(), searchQuery) {
			@Override
			protected void updateUI(final List<CallHistory> callHistories) {
				activity.runOnUiThread(new Runnable() {
					public void run() {
						// Notify adapter with new track list
						if (callsAdapter != null && callHistories != null && !callHistories.isEmpty()) {

							List<CallHistory> callHistoriesList = new ArrayList<CallHistory>();
							callHistoriesList.addAll(callHistories);

							callsAdapter.setCallHistories(callHistories);
							callsAdapter.notifyDataSetChanged();
						}
					}
				});
			}

			@Override
			protected void stopProgressBar() {
			}
		};
		getOfflineCallsByStatusBaseTask.execute();
	}

	protected abstract int getCallType();

	protected void getCalls(String searchQuery) {
		
		getOfflineCalls(0, searchQuery);
		/*
		final FragmentActivity activity = getActivity();
		ProgressDialog progressDialog = new ProgressDialog(activity);
		progressDialog.setMessage("Sync is in progress...");
		progressDialog.setCancelable(true);

		GetOfflineCallsByStatusBaseTask getOfflineCallsByStatusBaseTask = new GetOfflineCallsByStatusBaseTask(activity, progressDialog, 0, getCallType(), searchQuery) {
			@Override
			protected void updateUI(final List<CallHistory> callHistories) {
				activity.runOnUiThread(new Runnable() {
					public void run() {
						// Notify adapter with new track list
						if (callsAdapter != null && callHistories != null && !callHistories.isEmpty()) {

							List<CallHistory> callHistoriesList = new ArrayList<CallHistory>();
							callHistoriesList.addAll(callHistories);

							callsAdapter.setCallHistories(callHistories);
							callsAdapter.notifyDataSetChanged();
						}
					}
				});
			}

			@Override
			protected void stopProgressBar() {
				showEmptyText(getCallType(), getView(), CallsTab.this);
			}

		};
		getOfflineCallsByStatusBaseTask.execute();
	*/}

	private final void showEmptyText(int status, View view, ListFragment listFragment) {
		View root = view;
		if (root != null) {
			switch (status) {
			case Constants.MISSED:
				if (listFragment != null) {
					listFragment.setEmptyText("Missed calls not found...");
				}
				break;
			case Constants.INCOMING:
				if (listFragment != null) {
					listFragment.setEmptyText("Received calls not found...");
				}
				break;
			case Constants.OUTGOING:
				if (listFragment != null) {
					listFragment.setEmptyText("Dialled calls not found...");
				}
				break;

			default:
				break;
			}
		}
	}

	@Override
	public boolean onClose() {
		if (searchView != null) {
			searchView.setIconified(true);
		}

		this.searchQuery = null;
		resetPagination(this.searchQuery);
		return false;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		this.searchQuery = query;

		resetPagination(this.searchQuery);

		// your search methods
		if (searchView != null) {
			searchView.clearFocus();
		}
		return true;
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		if (newText == null || newText.isEmpty()) {
			this.searchQuery = null;
			resetPagination(this.searchQuery);
		}
		return false;
	}

	private void resetPagination(String searchQuery) {
		// Reset Tab context info
		callsAdapter.emptyList();
		callsAdapter.notifyDataSetChanged();
		endlessScrollListener.setCurrentPage(0);
		endlessScrollListener.setSearchQuery(searchQuery);

		// Get search result
		getCalls(searchQuery);

		// Signify that we are done refreshing
		if (swipeRefreshLayout != null) {
			swipeRefreshLayout.setRefreshing(false);
		}
	}

	@Override
	public void onRefresh() {
		// Start showing the refresh animation
		if (swipeRefreshLayout != null) {
			swipeRefreshLayout.setRefreshing(true);
			resetPagination(CallsTab.this.searchQuery);
		}

	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		// Context menu
		menu.setHeaderTitle("More Options");
		menu.add(Menu.NONE, CONTEXT_MENU_ADD_CONTACT, Menu.NONE, "Add/Update contact");
		menu.add(Menu.NONE, CONTEXT_MENU_CALL, Menu.NONE, "Call");
		menu.add(Menu.NONE, CONTEXT_MENU_SEND_SMS, Menu.NONE, "Send SMS");
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		CallHistory callHistory = new CallHistory();
		FragmentActivity activity = getActivity();
		if (activity instanceof TabsActivity) {
			ContextMenuInfo menuInfo = item.getMenuInfo();
			AdapterContextMenuInfo adapterContextMenuInfo = (AdapterContextMenuInfo) menuInfo;
			int position = adapterContextMenuInfo.position;
			int listSize = callsAdapter.getCallHistories().size();
			if (position < listSize) {
				callHistory = (CallHistory) callsAdapter.getItem(position);
			}

			if (getUserVisibleHint()) {
				switch (item.getItemId()) {
				case CONTEXT_MENU_ADD_CONTACT:
					String number = callHistory.getNumber();
					if (number != null) {
						Intent contactIntent = new Intent(Intents.Insert.ACTION);
						contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
						contactIntent.putExtra(Intents.Insert.PHONE, number);
						startActivityForResult(contactIntent, Constants.ADD_CONTACT);
					}
					return true;

				case CONTEXT_MENU_CALL:
					// call action
					String numberDail = callHistory.getNumber();
					if (numberDail != null) {
						Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + numberDail));
						startActivity(callIntent);
					}
					return true;

				case CONTEXT_MENU_SEND_SMS:
					// call action
					String sentSmsNumber = callHistory.getNumber();

					if (sentSmsNumber != null) {
						Intent sendIntent = new Intent(Intent.ACTION_SENDTO);
						sendIntent.setData(Uri.parse("sms:" + sentSmsNumber));
						startActivity(sendIntent);
					}
					return true;
				default:
					return super.onContextItemSelected(item);
				}
			}
		}
		return false;
	}
}
