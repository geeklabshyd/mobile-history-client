package com.geeklabs.callbackup.listfragment.tab;

import com.geeklabs.callbackup.util.Constants;

public class ReceivedCallsFragment extends CallsTab {

	@Override
	protected int getCallType() {
		return Constants.INCOMING;
	}

}

	