package com.geeklabs.callbackup.listfragment.tab;

import com.geeklabs.callbackup.util.Constants;

public class DialledCallsFragment extends CallsTab {

	@Override
	protected int getCallType() {
		return Constants.OUTGOING;
	}
}
