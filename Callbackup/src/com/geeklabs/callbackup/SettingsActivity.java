package com.geeklabs.callbackup;

import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;

import com.geeklabs.callbackup.adapter.PersonalizeListAdapter;
import com.geeklabs.callbackup.communication.task.post.PersonalizationTask;
import com.geeklabs.callbackup.util.Constants;
import com.google.android.gms.ads.AdView;

public class SettingsActivity extends ListActivity {

	private PersonalizeListAdapter listAdapter;
	private AdView adView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_personalization);

		// Action bar back ground color
		BitmapDrawable bitmap = (BitmapDrawable) getResources().getDrawable(R.drawable.steel_blue);
		bitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(bitmap);
		getActionBar().setDisplayHomeAsUpEnabled(true); // UP back navigation

		// set personalizeList here
		List<String> list = getPersonalizeList();
		listAdapter = new PersonalizeListAdapter(getApplicationContext());
		listAdapter.setPersonaliseList(list);
		setListAdapter(listAdapter);

		// Adds code
		// http://www.androidbegin.com/tutorial/integrating-new-google-admob-banner-interstitial-ads/
//		adView = (AdView) this.findViewById(R.id.adViewSettings);

//		adView.loadAd(AdRequestUtil.getAdRequest());
	}

	private List<String> getPersonalizeList() {
		List<String> list = new ArrayList<String>();
		list.add(Constants.SYNC_ONLY_ON_WI_FI_AVAILABLE);
		list.add(Constants.SYNC_ON_WI_FI_OR_MOBILE_DATA_AVAILABLE);
		list.add(Constants.SYNC_SMS);
		list.add(Constants.SYNC_MISSED_CALLS);
		list.add(Constants.SYNC_RECEIVED_CALLS);
		list.add(Constants.SYNC_DIALED_CALLS);
		list.add(Constants.SYNC_CONTACTS);
		list.add(Constants.SYNC_ALL);

		return list;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.personalization, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.save_settings:
			// call here async task for personalize
			savePersonalize();
			return true;
			// Respond to the action bar's Up/Home button
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void savePersonalize() {
		// create a progress dialog
		final ProgressDialog progressDialog = new ProgressDialog(SettingsActivity.this);
		progressDialog.setMessage("Please wait a while...");
		progressDialog.setCancelable(false);
		progressDialog.show();

		PersonalizationTask personalizationTask = new PersonalizationTask(progressDialog, SettingsActivity.this);
		personalizationTask.execute();

	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (adView != null) {
			adView.resume();
		}
	}

	/** Called when leaving the activity */
	@Override
	public void onPause() {
		if (adView != null) {
			adView.pause();
		}
		super.onPause();
	}

	/** Called before the activity is destroyed */
	@Override
	public void onDestroy() {
		if (adView != null) {
			adView.destroy();
		}
		super.onDestroy();
	}
}
