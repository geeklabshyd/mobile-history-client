package com.geeklabs.callbackup.sync.servertoapp.task;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.JSONException;

import android.app.Activity;
import android.app.ProgressDialog;

import com.geeklabs.callbackup.Preferences.AuthPreferences;
import com.geeklabs.callbackup.communication.AbstractHttpGetTask;
import com.geeklabs.callbackup.domain.CallHistory;
import com.geeklabs.callbackup.domain.SyncPreferences;
import com.geeklabs.callbackup.sqlite.SQLLiteManager;
import com.geeklabs.callbackup.util.RequestUrl;

public class SyncServerToAppCallsTask extends AbstractHttpGetTask {

	private Activity activity;
	private AuthPreferences authPreferences;
	private boolean stopSync;
	private int currentPage;
	private SyncPreferences syncPreferences;

	public SyncServerToAppCallsTask(ProgressDialog progressDialog, Activity activity, int currentPage, SyncPreferences syncPreferences) {
		super(progressDialog, activity);
		this.activity = activity;
		this.syncPreferences = syncPreferences;
		this.authPreferences = new AuthPreferences(activity);
		this.currentPage = currentPage;
	}

	@Override
	protected String getRequestUrl() {
		String replaceId = RequestUrl.GET_ALL_CALLS.replace("{id}", String.valueOf(authPreferences.getUserId()));
		String replacePageNoInReplaceId = replaceId.replace("{pageNo}", String.valueOf(currentPage));
		return replacePageNoInReplaceId;
	}

	@Override
	protected void showMessageOnUI(String string) {
		// no need to show msg it is background task
	}

	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		List<CallHistory> callHistories = new ArrayList<CallHistory>();
		SQLLiteManager sqlLiteManager = SQLLiteManager.getInstance(activity);
		if (!jsonResponse.isEmpty()) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				callHistories = mapper.readValue(jsonResponse, new TypeReference<List<CallHistory>>() {
				});
				
				if (!callHistories.isEmpty()) {
					//update server id
					for (CallHistory callHistory : callHistories) {
						Long serverCallId = callHistory.getServerCallId();
						if (callHistory.getServerCallId() != null && serverCallId > 0) {
							CallHistory callHistoryById = sqlLiteManager.getCallHistoryById(callHistory.getId());
							if (callHistoryById != null) {
								SQLLiteManager.getInstance(activity).updateOfflineCallHistory(callHistory);
							} else {
								SQLLiteManager.getInstance(activity).saveOfflineCallHistory(callHistory);
							}
						}
					}
				} else {
					// server to app deleted records sync
					
					stopSync = true;
					// Indicates calls sync has been finished
					syncPreferences.getProgressStatus().setSyncCall(true);
					syncPreferences.closeSpinnerIfAllTasksAreDone(activity);
					
					//ServerDeletedRecordsTask serverDeletedRecordsTask = new ServerDeletedRecordsTask(null, activity, 0);
					//serverDeletedRecordsTask.execute();
				}
				
				if (!stopSync) {
					currentPage++;
					SyncServerToAppCallsTask serverToAppCallsTask = new SyncServerToAppCallsTask(null, activity, currentPage, syncPreferences);
					serverToAppCallsTask.execute();
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				syncPreferences.getProgressStatus().setSyncCall(true);
				syncPreferences.closeSpinnerIfAllTasksAreDone(activity);
			} 
		}
	}

	@Override
	protected String getRequestJSON() {
		// need to create any json object
		return null;
	}
	
	@Override
	protected void addExtraParams(List<NameValuePair> params) {
		BasicNameValuePair nameValuePair = new BasicNameValuePair("currentPage", String.valueOf(currentPage));
		params.add(nameValuePair);
	}
}
