package com.geeklabs.callbackup.sync.servertoapp.task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.JSONException;

import android.app.Activity;
import android.app.ProgressDialog;

import com.geeklabs.callbackup.Preferences.AuthPreferences;
import com.geeklabs.callbackup.communication.AbstractHttpGetTask;
import com.geeklabs.callbackup.domain.DeletedRecords;
import com.geeklabs.callbackup.sqlite.SQLLiteManager;
import com.geeklabs.callbackup.util.RequestUrl;

public class ServerDeletedRecordsTask extends AbstractHttpGetTask {

	private Activity activity;
	private AuthPreferences authPreferences;
	private boolean stopSync;
	private int currentPage;

	public ServerDeletedRecordsTask(ProgressDialog progressDialog, Activity activity, int currentPage) {
		super(progressDialog, activity);
		this.activity = activity;
		this.authPreferences = new AuthPreferences(activity);
		this.currentPage = currentPage;
	}

	@Override
	protected String getRequestJSON() {
		return null;
	}

	@Override
	protected String getRequestUrl() {
		return RequestUrl.GET_SERVER_DELETED_RECORDS.replace("{id}", String.valueOf(authPreferences.getUserId()));
	}

	@Override
	protected void showMessageOnUI(String string) {
		// no need to show any msg
	}

	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		List<DeletedRecords> deletedRecords = new ArrayList<DeletedRecords>();
		SQLLiteManager sqlLiteManager = SQLLiteManager.getInstance(activity);
		if (!jsonResponse.isEmpty()) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				deletedRecords = mapper.readValue(jsonResponse, new TypeReference<List<DeletedRecords>>() {
				});
				
				if (!deletedRecords.isEmpty()) {
					//update server id
					for (DeletedRecords deletedRecord : deletedRecords) {
						Long serverId = deletedRecord.getServerId();
						if (serverId != null && serverId > 0) {
							//check in sqlite call table
							boolean isDeleted = sqlLiteManager.deleteOfflineCallHistory(serverId);
							if (!isDeleted) {
								//check in sms table
								sqlLiteManager.deleteOfflineSMSHistory(serverId);
							}
						}
					}
				} else {
					stopSync = true;
				}
				
				if (!stopSync) {
					currentPage++;
					ServerDeletedRecordsTask serverDeletedRecordsTask = new ServerDeletedRecordsTask(null, activity, currentPage);
					serverDeletedRecordsTask.execute();
				}
				
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void addExtraParams(List<NameValuePair> params) {
		BasicNameValuePair nameValuePair = new BasicNameValuePair("currentPage", String.valueOf(currentPage));
		params.add(nameValuePair);
	}
}
