package com.geeklabs.callbackup.sync.servertoapp.task;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.JSONException;

import android.app.Activity;
import android.app.ProgressDialog;

import com.geeklabs.callbackup.Preferences.AuthPreferences;
import com.geeklabs.callbackup.communication.AbstractHttpGetTask;
import com.geeklabs.callbackup.domain.SMS;
import com.geeklabs.callbackup.domain.SyncPreferences;
import com.geeklabs.callbackup.sqlite.SQLLiteManager;
import com.geeklabs.callbackup.util.RequestUrl;

public class SyncServerToAppSmsesTask extends AbstractHttpGetTask {

	private Activity activity;
	private AuthPreferences authPreferences;
	private boolean stopSync;
	private int currentPage;
	private SyncPreferences syncPreferences;

	public SyncServerToAppSmsesTask(ProgressDialog progressDialog, Activity activity, int currentPage, SyncPreferences syncPreferences) {
		super(progressDialog, activity);
		this.activity = activity;
		this.syncPreferences = syncPreferences;
		this.authPreferences = new AuthPreferences(activity);
		this.currentPage = currentPage;
	}

	@Override
	protected String getRequestUrl() {
		return RequestUrl.GET_SMS_HISTORY.replace("{id}", String.valueOf(authPreferences.getUserId()));
	}

	@Override
	protected void showMessageOnUI(String string) {
		// no need to show msg it is background task
	}

	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		List<SMS> smses = new ArrayList<SMS>();
		SQLLiteManager sqlLiteManager = SQLLiteManager.getInstance(activity);
		if (!jsonResponse.isEmpty()) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				smses = mapper.readValue(jsonResponse, new TypeReference<List<SMS>>() {
				});
				
				if (!smses.isEmpty()) {
					//update server id
					for (SMS sms : smses) {
						Long serverSmsId = sms.getServerSmsId();
						if (serverSmsId != null && serverSmsId > 0) {
							SMS smsById = sqlLiteManager.getSmsById(sms.getId());
							if (smsById != null) {
								SQLLiteManager.getInstance(activity).updateOfflineSMSHistory(sms);
							} else {
								SQLLiteManager.getInstance(activity).saveOfflineSMS(sms);
							}
						}
					}
				} else {
					// server to app deleted records sync
					stopSync = true;
					// Indicates sms sync has been finished
					syncPreferences.getProgressStatus().setSyncSms(true);
					syncPreferences.closeSpinnerIfAllTasksAreDone(activity);
					
					//ServerDeletedRecordsTask serverDeletedRecordsTask = new ServerDeletedRecordsTask(null, activity, 0);
					//serverDeletedRecordsTask.execute();
				}
				
				if (!stopSync) {
					currentPage++;
					SyncServerToAppSmsesTask serverToAppCallsTask = new SyncServerToAppSmsesTask(null, activity, currentPage, syncPreferences);
					serverToAppCallsTask.execute();
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				syncPreferences.getProgressStatus().setSyncSms(true);
				syncPreferences.closeSpinnerIfAllTasksAreDone(activity);
			} 
		}
	}

	@Override
	protected String getRequestJSON() {
		// need to create any json object
		return null;
	}
	
	@Override
	protected void addExtraParams(List<NameValuePair> params) {
		BasicNameValuePair nameValuePair = new BasicNameValuePair("currentPage", String.valueOf(currentPage));
		params.add(nameValuePair);
	}
}
