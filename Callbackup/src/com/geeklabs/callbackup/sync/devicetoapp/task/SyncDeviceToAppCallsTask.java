package com.geeklabs.callbackup.sync.devicetoapp.task;

import java.util.List;

import com.geeklabs.callbackup.SyncService;
import com.geeklabs.callbackup.domain.CallHistory;
import com.geeklabs.callbackup.domain.SyncPreferences;
import com.geeklabs.callbackup.sqlite.SQLLiteManager;
import com.geeklabs.callbackup.sync.apptoserver.task.SyncAppToServerCallsTask;
import com.geeklabs.callbackup.util.DeviceCalls;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.ProgressBar;

public class SyncDeviceToAppCallsTask extends AsyncTask<Void, ProgressBar, Void> {

	private SyncPreferences syncPreferences;
	private Activity activity;
	private SyncService syncService;
	private ProgressDialog progressDialog;

	
	
	public SyncDeviceToAppCallsTask(SyncPreferences syncPreferences, Activity activity, SyncService syncService) {
		this.syncPreferences = syncPreferences;
		this.activity = activity;
		this.syncService = syncService;
	}

	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
//		syncService.syncStarted(activity);
		progressDialog = new ProgressDialog(activity);
		progressDialog.setMessage("Sync is in progress please wait..");
		progressDialog.setCancelable(false);
		progressDialog.show();
	}
	@Override
	protected Void doInBackground(Void... params) {
		syncDeviceCallsAsAppCalls();
		return null;
	}

	private void syncDeviceCallsAsAppCalls() {
		// Get all Calls from Device.
		if ((syncPreferences.isSyncCall() || syncPreferences.isSyncAll())) {
			// get All Old calls from device
			List<CallHistory> allCallsFromDevice = DeviceCalls.getAllCallLogs(activity);
			SQLLiteManager.getInstance(activity).saveDeviceCalls(allCallsFromDevice);
		}
	}
	
	@Override
	protected void onPostExecute(Void result) {
//		syncService.syncCompleted();
		progressDialog.dismiss();
		SyncAppToServerCallsTask syncAppToServerCallsTask = new SyncAppToServerCallsTask(null, activity, syncPreferences);
		syncAppToServerCallsTask.execute();
	
	}
}
