package com.geeklabs.callbackup.sync.devicetoapp.task;

import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.ProgressBar;

import com.geeklabs.callbackup.SyncService;
import com.geeklabs.callbackup.domain.SMS;
import com.geeklabs.callbackup.domain.SyncPreferences;
import com.geeklabs.callbackup.sqlite.SQLLiteManager;
import com.geeklabs.callbackup.sync.apptoserver.task.SyncAppToServerSmsesTask;
import com.geeklabs.callbackup.util.DeviceSms;
import com.geeklabs.callbackup.util.SyncUtil;

public class SyncDeviceToAppSmsesTask extends AsyncTask<Void, ProgressBar, Void> {

	private SyncPreferences syncPreferences;
	private Activity activity;
	private SyncService syncService;
	private ProgressDialog progressDialog;

	public SyncDeviceToAppSmsesTask(SyncPreferences syncPreferences, Activity activity, SyncService syncService) {
		this.syncPreferences = syncPreferences;
		this.activity = activity;
		this.syncService = syncService;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
//		syncService.syncStarted(activity);
		
		progressDialog = new ProgressDialog(activity);
		progressDialog.setMessage("Sync is in progress please wait..");
		progressDialog.setCancelable(false);
		progressDialog.show();
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		syncDeviceSmsesAsAppSmses();
		return null;
	}

	private void syncDeviceSmsesAsAppSmses() {
		if ((syncPreferences.isSyncSms() || syncPreferences.isSyncAll())) {
			// get All old smses inbox and sent sms
			List<SMS> smsesFromDevice = DeviceSms.getAllInboxSmsesFromDevice(activity);
			SQLLiteManager.getInstance(activity).saveSmsFromDevice(smsesFromDevice);

			List<SMS> sentSmsesFromDevice = DeviceSms.getAllSentSmsesFromDevice(activity);
			SQLLiteManager.getInstance(activity).saveSmsFromDevice(sentSmsesFromDevice);
		}
}
	
	@Override
	protected void onPostExecute(Void result) {
//		syncService.syncCompleted();
		progressDialog.dismiss();
		SyncAppToServerSmsesTask  appToServerSmsesTask = new SyncAppToServerSmsesTask(null, activity, syncPreferences);
		appToServerSmsesTask.execute();
		
	}
}
