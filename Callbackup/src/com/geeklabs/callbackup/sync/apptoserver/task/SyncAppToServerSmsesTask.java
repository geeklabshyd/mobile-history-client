package com.geeklabs.callbackup.sync.apptoserver.task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.JSONException;

import android.app.Activity;
import android.app.ProgressDialog;

import com.geeklabs.callbackup.TabsActivity;
import com.geeklabs.callbackup.Preferences.AuthPreferences;
import com.geeklabs.callbackup.communication.AbstractHttpPostTask;
import com.geeklabs.callbackup.domain.SMS;
import com.geeklabs.callbackup.domain.SyncPreferences;
import com.geeklabs.callbackup.sqlite.SQLLiteManager;
import com.geeklabs.callbackup.sync.servertoapp.task.SyncServerToAppSmsesTask;
import com.geeklabs.callbackup.util.RequestUrl;

public class SyncAppToServerSmsesTask extends AbstractHttpPostTask {

	private Activity activity;
	private SyncPreferences syncPreferences;
	private AuthPreferences authPreferences;
	private boolean stopSync;

	public SyncAppToServerSmsesTask(ProgressDialog progressDialog, Activity activity, SyncPreferences syncPreferences) {
		super(progressDialog, activity);
		this.activity = activity;
		this.syncPreferences = syncPreferences;
		this.authPreferences = new AuthPreferences(activity);
	}

	@Override
	protected String getRequestJSON() {
		
		ObjectMapper mapper = new ObjectMapper();
		if (syncPreferences.isSyncAll() || syncPreferences.isSyncSms()) {
			// get calls where no serverId
			List<SMS> smses = SQLLiteManager.getInstance(activity).getOfflineSmsesWithoutServerIdByPagable(0);
			
			if (smses.isEmpty()) {
				stopSync = true;
			}
			
			try {
				return mapper.writeValueAsString(smses);
			} catch (JsonGenerationException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	protected String getRequestUrl() {
		return RequestUrl.SEND_SMS_HISTORY_REQ.replace("{id}", String.valueOf(authPreferences.getUserId()));
	}

	@Override
	protected void showMessageOnUI(String string) {
		// no need to show msg it is background task
	}

	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		List<SMS> smses = new ArrayList<SMS>();
		SQLLiteManager sqlLiteManager = SQLLiteManager.getInstance(activity);
		if (!jsonResponse.isEmpty()) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				smses = mapper.readValue(jsonResponse, new TypeReference<List<SMS>>() {
				});
				
				if (!smses.isEmpty()) {
					//update server id
					for (SMS sms : smses) {
						Long serverCallId = sms.getServerSmsId();
						if (sms.getServerSmsId() != null && serverCallId > 0) {
							SMS smsById = sqlLiteManager.getSmsById(sms.getId());
							if (smsById != null) {
								SQLLiteManager.getInstance(activity).updateOfflineSMSHistory(sms);
								
								TabsActivity.getMTabsAdaperToNofify().notifyDataSetChanged();
							}
						}
					}
				}
				
				if (!stopSync) {
					SyncAppToServerSmsesTask appToServerSmsesTask = new SyncAppToServerSmsesTask(progressDialog, activity, syncPreferences);
					appToServerSmsesTask.execute();
				} else {
					SyncServerToAppSmsesTask syncServerToAppSmsesTask = new SyncServerToAppSmsesTask(null, activity, 0, syncPreferences);
					syncServerToAppSmsesTask.execute();
				} 
			} catch (Exception e) {
				e.printStackTrace();
				syncPreferences.getProgressStatus().setSyncSms(true);
				syncPreferences.closeSpinnerIfAllTasksAreDone(activity);
			} 
		}
	}
}
