package com.geeklabs.callbackup.sync.apptoserver.task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.JSONException;

import android.app.Activity;
import android.app.ProgressDialog;

import com.geeklabs.callbackup.Preferences.AuthPreferences;
import com.geeklabs.callbackup.communication.AbstractHttpPostTask;
import com.geeklabs.callbackup.domain.CallHistory;
import com.geeklabs.callbackup.domain.SyncPreferences;
import com.geeklabs.callbackup.sqlite.SQLLiteManager;
import com.geeklabs.callbackup.sync.servertoapp.task.SyncServerToAppCallsTask;
import com.geeklabs.callbackup.util.RequestUrl;

public class SyncAppToServerCallsTask extends AbstractHttpPostTask {

	private Activity activity;
	private SyncPreferences syncPreferences;
	private AuthPreferences authPreferences;
	private boolean stopSync;

	public SyncAppToServerCallsTask(ProgressDialog progressDialog, Activity activity, SyncPreferences syncPreferences) {
		super(progressDialog, activity);
		this.activity = activity;
		this.syncPreferences = syncPreferences;
		this.authPreferences = new AuthPreferences(activity);
	}

	@Override
	protected String getRequestJSON() {
		
		ObjectMapper mapper = new ObjectMapper();
		if (syncPreferences.isSyncAll() || syncPreferences.isSyncCall()) {
			// get calls where no serverId
			List<CallHistory> callHistories = SQLLiteManager.getInstance(activity).getOfflineCallsWithoutServerIdByPagable(0);
			
			if (callHistories.isEmpty()) {
				stopSync = true;
			}
			
			try {
				return mapper.writeValueAsString(callHistories);
			} catch (JsonGenerationException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	protected String getRequestUrl() {
		return RequestUrl.SEND_CALL_HISTORY_REQ.replace("{id}", String.valueOf(authPreferences.getUserId()));
	}

	@Override
	protected void showMessageOnUI(String string) {
		// no need to show msg it is background task
	}

	@Override
	protected void handleResponse(String jsonResponse) throws JSONException {
		List<CallHistory> callHistories = new ArrayList<CallHistory>();
		SQLLiteManager sqlLiteManager = SQLLiteManager.getInstance(activity);
		if (!jsonResponse.isEmpty()) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				callHistories = mapper.readValue(jsonResponse, new TypeReference<List<CallHistory>>() {
				});
				
				if (!callHistories.isEmpty()) {
					//update server id
					for (CallHistory callHistory : callHistories) {
						Long serverCallId = callHistory.getServerCallId();
						if (callHistory.getServerCallId() != null && serverCallId > 0) {
							CallHistory callHistoryById = sqlLiteManager.getCallHistoryById(callHistory.getId());
							if (callHistoryById != null) {
								SQLLiteManager.getInstance(activity).updateOfflineCallHistory(callHistory);
							}
						}
					}
				}
				
				if (!stopSync) {
					// sync app to server
					SyncAppToServerCallsTask appToServerCallsTask = new SyncAppToServerCallsTask(null, activity, syncPreferences);
					appToServerCallsTask.execute();
				} else {
					//sync server to app 
					SyncServerToAppCallsTask syncServerToAppCallsTask = new SyncServerToAppCallsTask(null, activity, 0, syncPreferences);
					syncServerToAppCallsTask.execute();
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				syncPreferences.getProgressStatus().setSyncCall(true);
				syncPreferences.closeSpinnerIfAllTasksAreDone(activity);
			} 
		}
	}
}
