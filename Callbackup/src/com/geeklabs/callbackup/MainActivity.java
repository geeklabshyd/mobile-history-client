package com.geeklabs.callbackup;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;

import com.geeklabs.callbackup.Preferences.AuthPreferences;
import com.geeklabs.callbackup.oauth.AuthActivity;
import com.geeklabs.callbackup.util.NetworkService;
import com.google.android.gms.common.SignInButton;

public class MainActivity extends Activity {
	private AuthPreferences authPreferences;
	private static final String TAG = "MainActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Action bar back ground color
		BitmapDrawable bitmap = (BitmapDrawable) getResources().getDrawable(R.drawable.steel_blue);
		bitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
		getActionBar().setBackgroundDrawable(bitmap);
		
		authPreferences = new AuthPreferences(getApplicationContext());
		setContentView(R.layout.activity_home);
		

		final SignInButton sign_in_button = (SignInButton) findViewById(R.id.sign_in_button);

		sign_in_button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!NetworkService.isNetWorkAvailable(MainActivity.this)) {
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
					// set title
					alertDialogBuilder.setTitle("Warning");
					alertDialogBuilder.setMessage("Check your network connection and try again.");
					// set dialog message
					alertDialogBuilder.setCancelable(false).setNegativeButton("Ok", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
						}
					});
					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();

					// show it
					alertDialog.show();
				} else if (authPreferences.isUserSignedIn() && authPreferences.getUserId() > 0) {
					Log.i(TAG, "User already signed in");

					Intent intent = new Intent(getApplicationContext(), TabsActivity.class);
					startActivity(intent);
					finish();
				} else {
					Intent intent = new Intent(getApplicationContext(), AuthActivity.class);
					startActivity(intent);
					finish();
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;

	}

	@Override
	protected void onResume() {
		super.onResume();
		if (authPreferences.isUserSignedIn() && authPreferences.getUserId() > 0) {
			Log.i(TAG, "OnResume User already signed in");
			Intent intent = new Intent(getApplicationContext(), TabsActivity.class);
			startActivity(intent);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		moveTaskToBack(true);
	}
}
