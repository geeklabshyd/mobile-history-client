package com.geeklabs.callbackup;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class HelpActivity extends Activity {
	TextView tv1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_url);

		// Action bar back ground color
		BitmapDrawable bitmap = (BitmapDrawable) getResources().getDrawable(R.drawable.steel_blue);
		bitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
		getActionBar().setBackgroundDrawable(bitmap);
		getActionBar().setDisplayHomeAsUpEnabled(true); // UP back navigation

		String str = "My Mobi Sync &nbsp;&nbsp;&nbsp;<br>"

		+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>  Backup and Sync your Mobile SMS, Calls and Contacts. <br> Why 'SMS Call n Contacts Backup' ? <br> * Without mobile at hand, You can monitor all incoming SMS and Calls by simply logging into your account https://smsandcallhistory.appspot.com"
				+ " <br>* Supports Offline mode "
				+ " <br>* You can get back all your Contacts, SMS and Calls into your New mobile"
				+ "<br>* You can Sync all your Contacts, SMS and Calls back and fourth between your mobile and server"
				+ "<br>* Completely Free.";

		str = str + "<br> How it works:" + "<br>1. Install the app and enable mobile data or Wi-Fi"
				+ "<br>2. Login using Email ID" + "<br>3. Sync all your Contacts, SMS and Calls to server."
				+ "<br>4. Access application through https://smsandcallhistory.appspot.com  <br>";

		String[] str_array = str.split(" ");

		for (int i = 0; i < str_array.length; i++) {
			if (str_array[i].startsWith("1")) {
				str = str.replaceAll(str_array[i], "<font color='blue'>" + str_array[i] + "</font>");
			}
		}

		tv1 = (TextView) findViewById(R.id.help_text_view);

		TextView helpUrl = (TextView) findViewById(R.id.help_text);
		tv1.setText(Html.fromHtml(str));
		helpUrl.setText(Html.fromHtml("<font color='blue'> http://smsandcallhistory.appspot.com</font> <br><br><br><br>"));
		// tv1.setText(Html.fromHtml());
		onClickOnUrlTextView(helpUrl);
	}

	private void onClickOnUrlTextView(final TextView helpUrl) {
		helpUrl.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String url = helpUrl.getText().toString();
				Uri uri = Uri.parse(url);
				Intent intent = new Intent(Intent.ACTION_VIEW, uri);
				startActivity(intent);
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
