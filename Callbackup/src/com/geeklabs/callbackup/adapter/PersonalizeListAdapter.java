package com.geeklabs.callbackup.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.geeklabs.callbackup.R;
import com.geeklabs.callbackup.Preferences.PerPreferences;
import com.geeklabs.callbackup.util.Constants;

public class PersonalizeListAdapter extends BaseAdapter {
	private Context context;
	private PerPreferences perPreferences;

	private List<String> personaliseList = new ArrayList<String>();
	private String personalizeType;
	private LayoutInflater inflater;

	public void setPersonaliseList(List<String> personaliseList) {
		this.personaliseList.clear();
		this.personaliseList = personaliseList;
	}

	public List<String> getPersonaliseList() {
		return personaliseList;
	}

	public PersonalizeListAdapter(Context context) {
		this.context = context;
		perPreferences = new PerPreferences(context);
	}

	@Override
	public int getCount() {
		return personaliseList.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		personalizeType = personaliseList.get(position);
		if (convertView == null) {
			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.listadapter, null);
			holder = new ViewHolder();

			holder.checkBox = (CheckBox) convertView.findViewById(R.id.isTaken);
			holder.personalizeTextView = (TextView) convertView.findViewById(R.id.option);
			// set text view
			holder.personalizeTextView.setText(personalizeType);
			holder.personalizeTextView.setTextColor(Color.BLACK);
//			holder.checkBox.setTextColor(Color.LTGRAY);

			// set status of personalize
			if (personalizeType.equals(Constants.SYNC_SMS)) {
				if (perPreferences.isMessageStatus()) {
					holder.checkBox.setChecked(true);
				}
			} else if (personalizeType.equals(Constants.SYNC_MISSED_CALLS)) {
				if (perPreferences.isMissedCallStatus()) {
					holder.checkBox.setChecked(true);
				}
			} else if (personalizeType.equals(Constants.SYNC_RECEIVED_CALLS)) {
				if (perPreferences.isReceivedCallStatus()) {
					holder.checkBox.setChecked(true);
				}
			} else if (personalizeType.equals(Constants.SYNC_DIALED_CALLS)) {
				if (perPreferences.isDialedCallStatus()) {
					holder.checkBox.setChecked(true);
				}
			} else if (personalizeType.equals(Constants.SYNC_ALL)) {
				if (perPreferences.isAllPersonalized()) {
					holder.checkBox.setChecked(true);
				}
			} else if (personalizeType.equals(Constants.SYNC_ONLY_ON_WI_FI_AVAILABLE)) {
				if (perPreferences.isSyncOnlyWifi()) {
					holder.checkBox.setChecked(true);
				}
			} else if (personalizeType.equals(Constants.SYNC_CONTACTS)) {
				if (perPreferences.isContactsEnabled()) {
					holder.checkBox.setChecked(true);
				}
			} else if (personalizeType.equals(Constants.SYNC_ON_WI_FI_OR_MOBILE_DATA_AVAILABLE)) {
				if (perPreferences.isSyncWifiOrData()) {
					holder.checkBox.setChecked(true);
				}
			}
			
			holder.checkBox.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {

					if (personaliseList.get(position).equals(Constants.SYNC_SMS)) {
						perPreferences.setMessageStatus(holder.checkBox.isChecked());
					} else if (personaliseList.get(position).equals(Constants.SYNC_MISSED_CALLS)) {
						perPreferences.setMissedCallStatus(holder.checkBox.isChecked());
					} else if (personaliseList.get(position).equals(Constants.SYNC_RECEIVED_CALLS)) {
						perPreferences.setReceivedCallStatus(holder.checkBox.isChecked());
					} else if (personaliseList.get(position).equals(Constants.SYNC_DIALED_CALLS)) {
						perPreferences.setDialedCallStatus(holder.checkBox.isChecked());
					} else if (personaliseList.get(position).equals(Constants.SYNC_ALL)) {
						perPreferences.setAllPersonizeStatus(holder.checkBox.isChecked());
					} else if (personaliseList.get(position).equals(Constants.SYNC_ONLY_ON_WI_FI_AVAILABLE)) {
						perPreferences.setSyncWifiOnly(holder.checkBox.isChecked());
					} else if (personaliseList.get(position).equals(Constants.SYNC_CONTACTS)) {
						perPreferences.setContactsEnabled(holder.checkBox.isChecked());
					} else if (personaliseList.get(position).equals(Constants.SYNC_ON_WI_FI_OR_MOBILE_DATA_AVAILABLE)) {
						perPreferences.setSyncWifiOrData(holder.checkBox.isChecked());
					}
				}
			});

		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		return convertView;
	}

	static class ViewHolder {
		CheckBox checkBox;
		TextView personalizeTextView;
	}
}
