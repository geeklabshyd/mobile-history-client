package com.geeklabs.callbackup.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.geeklabs.callbackup.R;
import com.geeklabs.callbackup.domain.CallHistory;

public class CallsAdapter extends BaseAdapter {

	private List<CallHistory> callHistories = new ArrayList<CallHistory>();
	private CallHistory callHistory;
	private LayoutInflater inflater;
	private Activity activity;

	public void setCallHistories(List<CallHistory> callHistories) {
		if (this.callHistories.size() > 0) {
			this.callHistories.addAll(callHistories);
		} else {
			this.callHistories = callHistories;
		}
	}

	public void emptyList() {
		this.callHistories.clear();
	}
	
	public List<CallHistory> getCallHistories() {
		return callHistories;
	}

	public CallsAdapter(List<CallHistory> callHistories, Context context) {
		this.callHistories.clear();
		this.callHistories = callHistories;
	}

	public CallsAdapter(Activity activity) {
		this.inflater = LayoutInflater.from(activity);
		this.activity = activity;
	}

	@Override
	public int getCount() {
		return callHistories.size();
	}

	@Override
	public Object getItem(int position) {
		return callHistories.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		callHistory = callHistories.get(position);
		final ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			 convertView = inflater.inflate(R.layout.call_list, null);

			// contect text view
			holder.contactNameTextView = (TextView) convertView.findViewById(R.id.contact_name_textview);

			// mobile number text view
			holder.mobileTextView = (TextView) convertView.findViewById(R.id.mobile_textview);

			// date
			holder.dateTextView = (TextView) convertView.findViewById(R.id.date_textview); 
			
			// callDuration
//			holder.callDurationTextView = (TextView) convertView.findViewById(R.id.duration); 

			// image for menu
			holder.arrowImageView = (ImageView) convertView.findViewById(R.id.arrow_down);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.contactNameTextView.setText(callHistory.getContactName());
		holder.mobileTextView.setText(callHistory.getNumber());
		holder.dateTextView.setText(callHistory.getDate());
		// on click on image
		holder.arrowImageView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				activity.openContextMenu(v);
				activity.registerForContextMenu(v);
			}
		});
		return convertView;
	}
	
	private static class ViewHolder {
		TextView contactNameTextView, dateTextView, mobileTextView, callDurationTextView;
		ImageView arrowImageView;
	}
}
