package com.geeklabs.callbackup.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.geeklabs.callbackup.R;
import com.geeklabs.callbackup.domain.SMS;
import com.geeklabs.callbackup.util.Constants;

public class SmsAdapter extends BaseAdapter {

	private List<SMS> smsList = new ArrayList<SMS>();
	private SMS sms;
	private LayoutInflater inflater;
	private Activity activity;

	public void setSMS(List<SMS> smsList) {
		if (this.smsList.size() > 0) {
			this.smsList.addAll(smsList);
		} else {
			this.smsList = smsList;
		}
	}

	public List<SMS> getsms() {
		return smsList;
	}

	public SmsAdapter(List<SMS> sms, Context context) {
		this.smsList.clear();
		this.smsList = sms;
	}

	public SmsAdapter(Activity activity) {
		this.inflater = LayoutInflater.from(activity);
		this.activity = activity;
	}

	public void emptyList() {
		this.smsList.clear();
	}
	
	@Override
	public int getCount() {
		return smsList.size();
	}

	@Override
	public Object getItem(int position) {
		return smsList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		sms = smsList.get(position);
		final ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.sms_row, null);

			// contect text view
			holder.contactNameTextView = (TextView) convertView.findViewById(R.id.sms_contact_textView);

			holder.dateTextView = (TextView) convertView.findViewById(R.id.sms_date_textView);

			
			// mobile number text view
			holder.mobileTextView = (TextView) convertView.findViewById(R.id.sms_mobile_textView);
			
            // text
			holder.smsTextView = (TextView) convertView.findViewById(R.id.sms_text_view);
			
			// Image type
			holder.receivedImage = (ImageView)convertView.findViewById(R.id.sms_received);
			holder.deliveredImage = (ImageView)convertView.findViewById(R.id.sms_delivered);
			holder.smsRowImage = (ImageView)convertView.findViewById(R.id.sms_button);
			
			
			holder.smsRowImage.setOnClickListener(new OnClickListener() {
				

				@Override
				public void onClick(View v) {
					activity.openContextMenu(v);
					activity.registerForContextMenu(v);
				}
			});
			
			
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		String contactName = sms.getContactName();
		holder.contactNameTextView.setText(contactName);
		holder.dateTextView.setText(sms.getDate());
		holder.mobileTextView.setText(sms.getFrom());
		holder.smsTextView.setText(sms.getMessage());
		if (sms.getType() == Constants.SMS_RECEIVED) {
			holder.deliveredImage.setVisibility(View.GONE);
			holder.receivedImage.setVisibility(View.VISIBLE);
		} else  {
			holder.deliveredImage.setVisibility(View.VISIBLE);
			holder.receivedImage.setVisibility(View.GONE);
		}
		return convertView;
	}

	private static class ViewHolder {
		public ImageView smsRowImage;
		TextView contactNameTextView, dateTextView, mobileTextView,smsTextView;
		ImageView receivedImage, deliveredImage;
	}
}
