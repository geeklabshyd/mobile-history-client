package com.geeklabs.callbackup;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.geeklabs.callbackup.Preferences.AuthPreferences;
import com.geeklabs.callbackup.communication.task.get.SignOutTask;
import com.geeklabs.callbackup.communication.task.post.ImageLoadTask;
import com.geeklabs.callbackup.util.AlertDialogPopup;
import com.geeklabs.callbackup.util.NetworkService;
import com.google.android.gms.ads.AdView;

public class ProfileActivity extends Activity {
	private TextView userNameTestView;
	private TextView emailTestView;
	private ImageView profile_photo;
	private AuthPreferences authPreferences;
	private AdView adView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// overridePendingTransition(R.anim.animin, R.anim.animout);
		// this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.homeactivity);

		// Action bar back ground color
		BitmapDrawable bitmap = (BitmapDrawable) getResources().getDrawable(R.drawable.steel_blue);
		bitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
		getActionBar().setBackgroundDrawable(bitmap);
		getActionBar().setDisplayHomeAsUpEnabled(true); // UP back navigation

		Button findViewById = (Button) findViewById(R.id.signoutbutton1);
		authPreferences = new AuthPreferences(getApplicationContext());

		String userName = authPreferences.getUserName();
		String email = authPreferences.getEmail();
		String picUrl = null;
		picUrl = authPreferences.getPicUrl();

		profile_photo = (ImageView) findViewById(R.id.login);

		userNameTestView = (TextView) findViewById(R.id.userName);
		emailTestView = (TextView) findViewById(R.id.email);

		userNameTestView.setText("  " + userName);
		emailTestView.setText("  " + email);
		if (NetworkService.isNetWorkAvailable(getApplicationContext())) {
			new ImageLoadTask(picUrl, profile_photo).execute();
		} else {
			profile_photo.setVisibility(View.VISIBLE);
		}

		userNameTestView.setTextColor(Color.BLUE);
		emailTestView.setTextColor(Color.BLUE);

		// Adds code
		// http://www.androidbegin.com/tutorial/integrating-new-google-admob-banner-interstitial-ads/
		adView = (AdView) this.findViewById(R.id.adViewMyAccount);

		adView.loadAd(AdRequestUtil.getAdRequest());

		onclickOnSignOutButton(findViewById);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void onclickOnSignOutButton(Button signOutButton) {
		signOutButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (NetworkService.isNetWorkAvailable(ProfileActivity.this)) {
					// just call here sign out method
					signoutUser();
				} else {
					AlertDialog showAlertDialog = AlertDialogPopup.showAlertDialog("Network warning!", "Check your network connection and try again.", "Ok", null,
							ProfileActivity.this, null, null);
					showAlertDialog.show();
				}
			}
		});
	}

	private void signoutUser() {
		final ProgressDialog signoutAPKProgressDialog = new ProgressDialog(ProfileActivity.this);
		signoutAPKProgressDialog.setMessage("Signout is in progress, please wait...");
		signoutAPKProgressDialog.setCancelable(false);
		signoutAPKProgressDialog.show();

		if (authPreferences.isUserSignedIn() && authPreferences.getUserAccount() != null && authPreferences.getUserId() > 0) {
			SignOutTask signOutTask = new SignOutTask(signoutAPKProgressDialog, getApplicationContext(), ProfileActivity.this);
			signOutTask.execute();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (adView != null) {
			adView.resume();
		}
	}

	/** Called when leaving the activity */
	@Override
	public void onPause() {
		if (adView != null) {
			adView.pause();
		}
		super.onPause();
	}

	/** Called before the activity is destroyed */
	@Override
	public void onDestroy() {
		if (adView != null) {
			adView.destroy();
		}
		super.onDestroy();
	}
}
