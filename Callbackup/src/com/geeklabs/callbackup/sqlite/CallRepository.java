package com.geeklabs.callbackup.sqlite;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.geeklabs.callbackup.domain.CallHistory;
import com.geeklabs.callbackup.util.Constants;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class CallRepository {

	public void saveCallHistory(CallHistory call, SQLiteDatabase db) {

		if (!isCallExistByDateAndType(call, db)) { // Is record exist ???
			StringBuilder records = new StringBuilder("INSERT INTO" + " " + SQLLite.CALL_HISTORY_TABLE_NAME
					+ "(serverCallId, userId, number, date, time, callType, " + "contactName, isSyncked)values(");

			// get serverCall id and check it
			Long serverCallId = call.getServerCallId();
			if (serverCallId != null && serverCallId > 0) {
				records.append(serverCallId + ",");
			} else {
				records.append("null, ");
			}

			String contactName = call.getContactName();
			if (contactName.contains("'")) {
				contactName = contactName.replaceAll("'", "''");
			}

			records.append(call.getUserId() + "," + "'" + call.getNumber() + "'," + "'" + call.getDate() + "'," + "'"
					+ call.getTime() + "'," + "'" + call.getCallType() + "'," + "'" + contactName + "',");

			int isSynced = call.isSyncked() ? 1 : 0;
			records.append("'" + isSynced + "'");

			db.execSQL(records.toString() + ")");

			Log.i("Offline Track info", "" + SQLLite.CALL_HISTORY_TABLE_NAME + " call info successfully saved -> "
					+ "Number: " + call.getNumber() + "contact Name:" + call.getContactName());
		}
	}

	public boolean isCallExistByDateAndType(CallHistory call, SQLiteDatabase db) {
		Cursor cursor = db.rawQuery("SELECT * FROM " + SQLLite.CALL_HISTORY_TABLE_NAME + " where date='"
				+ call.getDate() + "' and callType='" + call.getCallType() + "'", null);
		boolean moveToFirst = cursor.moveToFirst();
		cursor.close();
		return moveToFirst;
	}

	public List<CallHistory> getPaginationAllCallsByStatus(int currentPage, int type, SQLiteDatabase db,
			String searchTerm) {
		int offset = 0;
		if (currentPage > 0) {
			offset = SQLLite.limit * currentPage;
		}

		if (searchTerm != null && !searchTerm.isEmpty()) {
			Cursor cursor = db.query(SQLLite.CALL_HISTORY_TABLE_NAME, null,
					" callType =? and (number LIKE ? or contactName LIKE ?)",
					new String[] { "" + type, "%" + searchTerm + "%", "%" + searchTerm + "%" }, null, null, "substr(date, 7,4) DESC, substr(date, 4, 2) DESC, substr(date, 1,2) DESC, substr(date, 12, 2) DESC, substr(date, 15, 2) DESC, substr(date, 18,2) DESC ",
					" " + offset + ", " + SQLLite.limit);
			return extractCallsFromCursor(cursor);
		}

		Cursor cursor = db.query(SQLLite.CALL_HISTORY_TABLE_NAME, null, " callType =?", new String[] { "" + type },
				null, null,
				" substr(date, 7,4) DESC, substr(date, 4, 2) DESC, substr(date, 1,2) DESC, substr(date, 12, 2) DESC, substr(date, 15, 2) DESC, substr(date, 18,2) DESC ",
				" " + offset + ", " + SQLLite.limit);
		
		return extractCallsFromCursor(cursor);
	}

	public List<CallHistory> getAllOfflineCalls(SQLiteDatabase db) {
		 Cursor cursor = db.query(SQLLite.CALL_HISTORY_TABLE_NAME, null, null,
		 null, null, null, "substr(date, 7,4) DESC, substr(date, 4, 2) DESC, substr(date, 1,2) DESC, substr(date, 12, 2) DESC, substr(date, 15, 2) DESC, substr(date, 18,2) DESC ");
		return extractCallsFromCursor(cursor);
	}

	public void updateOfflineCallHistory(CallHistory callHistory, SQLiteDatabase db) {
		Long serverCallId = callHistory.getServerCallId();
		int isSyncked = callHistory.isSyncked() ? 1 : 0;
		if (serverCallId != null && serverCallId > 0) {
			db.execSQL("UPDATE " + SQLLite.CALL_HISTORY_TABLE_NAME + " set serverCallId= '" + serverCallId
					+ "',isSyncked= '" + isSyncked + "' where id = '" + callHistory.getId() + "'");
			Log.i("Update Offline call", "call is successfully updated in with server call id");
		}
	}

	public boolean deleteOfflineCallHistory(Long id, SQLiteDatabase db) {

		if (id != null && id > 0) {
			db.execSQL("DELETE FROM " + SQLLite.CALL_HISTORY_TABLE_NAME + " WHERE serverCallId = " + id + "");
			return true;
		}
		return false;
	}

	public void deleteAllCalls(SQLiteDatabase db) {
		db.execSQL("DELETE FROM " + SQLLite.CALL_HISTORY_TABLE_NAME + "");
	}

	public List<CallHistory> getAllOfflineCallsWithoudServerCallId(SQLiteDatabase db) {
		/*
		 * Cursor cursor = db.query(SQLLite.CALL_HISTORY_TABLE_NAME, null,
		 * " serverCallId is null or serverCallId=?", new String[] { "" }, null,
		 * null, null, null);
		 */
		Cursor cursor = db.query(SQLLite.CALL_HISTORY_TABLE_NAME, null, " serverCallId is null or serverCallId=?",
				new String[] { "" }, null, null, "substr(date, 7,4) DESC, substr(date, 4, 2) DESC, substr(date, 1,2) DESC, substr(date, 12, 2) DESC, substr(date, 15, 2) DESC, substr(date, 18,2) DESC ");

		return extractCallsFromCursor(cursor);
	}

	public CallHistory getCallHistoryById(int id, SQLiteDatabase db) {
		Cursor cursor = db.rawQuery("SELECT * FROM " + SQLLite.CALL_HISTORY_TABLE_NAME + " where id='" + id + "'",
				null);
		if (cursor.moveToFirst()) {
			CallHistory extractCallFromCursor = extractCallFromCursor(cursor);
			cursor.close();
			return extractCallFromCursor;
		}
		cursor.close();
		return null;
	}

	public List<CallHistory> getOfflineCallsWithoutServerIdByPagable(int currentPage, SQLiteDatabase db) {
		int offset = 0;
		if (currentPage > 0) {
			offset = SQLLite.limit * currentPage;
		}

		Cursor cursor = db.query(SQLLite.CALL_HISTORY_TABLE_NAME, null, " serverCallId is null or serverCallId=?",
				new String[] { "" }, null, null, "substr(date, 7,4) DESC, substr(date, 4, 2) DESC, substr(date, 1,2) DESC, substr(date, 12, 2) DESC, substr(date, 15, 2) DESC, substr(date, 18,2) DESC ", " " + offset + ", " + SQLLite.limit);
		return extractCallsFromCursor(cursor);
	}

	private List<CallHistory> extractCallsFromCursor(Cursor cursor) {
		List<CallHistory> callHistories = new ArrayList<CallHistory>();
		cursor.moveToFirst();
		while (cursor.isAfterLast() == false) {
			CallHistory callHistory = extractCallFromCursor(cursor);
			callHistories.add(callHistory);
			cursor.moveToNext();
		}
		cursor.close();
		Log.i("Get Call info by status ", "Successfully got existing calls");
		return callHistories;
	}

	private CallHistory extractCallFromCursor(Cursor cursor) {
		CallHistory callHistory = new CallHistory();

		if (cursor.getInt(0) > 0) {
			callHistory.setId(cursor.getInt(0));
		}

		Long serverSideCallId = cursor.getLong(1);

		if (serverSideCallId != null && serverSideCallId > 0) {
			callHistory.setServerCallId(serverSideCallId);
		}

		callHistory.setUserId(cursor.getLong(2));
		callHistory.setNumber(cursor.getString(3));

		// for date
		SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_SAVE_FORMAT);
		String date = cursor.getString(4);
		try {
			Date parsedDate = format.parse(date);
			callHistory.setDate(format.format(parsedDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		callHistory.setTime(cursor.getString(5));
		callHistory.setCallType(cursor.getInt(6));
		callHistory.setContactName(cursor.getString(7));
		callHistory.setSyncked(cursor.getInt(8) > 0);

		return callHistory;
	}
}
