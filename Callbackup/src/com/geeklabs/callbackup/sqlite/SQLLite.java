package com.geeklabs.callbackup.sqlite;

public class SQLLite {
	public static final String CALL_HISTORY_TABLE_NAME = "CallHistory";
	public static final String SMS_HISTORY_TABLE_NAME = "SMSHistory";
	
	public static final int limit = 200;
}
