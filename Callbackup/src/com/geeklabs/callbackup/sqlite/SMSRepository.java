package com.geeklabs.callbackup.sqlite;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.geeklabs.callbackup.domain.SMS;
import com.geeklabs.callbackup.util.Constants;

public class SMSRepository {

	public void saveOfflineSMS(SMS sms, SQLiteDatabase db) {
		// Check whether sms exist with date and type ??
		SMS smsByDateAndType = getSmsByDateAndType(sms, db);
		if (smsByDateAndType == null) {

			StringBuilder records = new StringBuilder("INSERT INTO" + " " + SQLLite.SMS_HISTORY_TABLE_NAME
					+ "(serverSmsId, userId, contactNumber, message, contactName, date, " + "isSyncked, type)values(");

			// get serverCall id and check it
			Long serverSmsId = sms.getServerSmsId();
			if (serverSmsId != null && serverSmsId > 0) {
				records.append(serverSmsId + ",");
			} else {
				records.append("null, ");
			}

			String contactName = sms.getContactName();
			if (contactName.contains("'")) {
				contactName = contactName.replaceAll("'", "''");
			}
			
			
			String message = sms.getMessage();
			message = message.replace('\'', '`');
			records.append(sms.getUserId() + "," + "'" + sms.getFrom() + "'," + "'" + message + "'," + "'" + contactName + "'," + "'"
					+ sms.getDate() + "',");

			int isSynced = sms.isSyncked() ? 1 : 0;
			records.append("'" + isSynced + "','" + sms.getType() + "'");
			db.execSQL(records.toString() + ")");

			Log.i("Offline sms info", "" + SQLLite.SMS_HISTORY_TABLE_NAME + "sms info successfully saved -> " + "Number: " + sms.getFrom()
					+ "contact Name:" + sms.getContactName());
		}
	}

	public List<SMS> getPaginationAllSmses(int currentPage, SQLiteDatabase db, String searchTerm) {
		int offset = 0;
		if (currentPage > 0) {
			offset = SQLLite.limit * currentPage;
		}
		if (searchTerm != null && !searchTerm.isEmpty()) {

			Cursor cursor = db.query(SQLLite.SMS_HISTORY_TABLE_NAME, null, "(contactNumber LIKE ? or contactName LIKE ?)", new String[] {
					"%" + searchTerm + "%", "%" + searchTerm + "%" }, null, null, "substr(date, 7,4) DESC, substr(date, 4, 2) DESC, substr(date, 1,2) DESC, substr(date, 12, 2) DESC, substr(date, 15, 2) DESC, substr(date, 18,2) DESC ", " " + offset + ", " + SQLLite.limit);
			return extractSMSesFromCursor(cursor);
		}
		Cursor cursor = db.query(SQLLite.SMS_HISTORY_TABLE_NAME, null, null, null, null, null, "substr(date, 7,4) DESC, substr(date, 4, 2) DESC, substr(date, 1,2) DESC, substr(date, 12, 2) DESC, substr(date, 15, 2) DESC, substr(date, 18,2) DESC ", " " + offset + ", " + SQLLite.limit);
		return extractSMSesFromCursor(cursor);
	}

	private List<SMS> extractSMSesFromCursor(Cursor cursor) {
		List<SMS> smses = new ArrayList<SMS>();
		cursor.moveToFirst();
		while (cursor.isAfterLast() == false) {
			SMS sms = new SMS();

			if (cursor.getInt(0) > 0) {
				sms.setId(cursor.getInt(0));
			}
			Long serverSideSmsId = cursor.getLong(1);
			
			if (serverSideSmsId != null && serverSideSmsId > 0 ) {
				sms.setServerSmsId(serverSideSmsId);
			}

			sms.setUserId(cursor.getLong(2));
			sms.setFrom(cursor.getString(3));
			sms.setMessage(cursor.getString(4));
			sms.setContactName(cursor.getString(5));
			sms.setDate(cursor.getString(6));
			sms.setSyncked(cursor.getInt(7) > 0);
			sms.setType(cursor.getInt(8));

			smses.add(sms);
			cursor.moveToNext();
		}
		cursor.close();
		Log.i("Get Smses info ", "Successfully got existing Smses");
		return smses;
	}

	public void updateOfflineSMSHistory(SMS sms, SQLiteDatabase db) {
		Long serverSmsId = sms.getServerSmsId();
		int isSyncked = sms.isSyncked() ? 1 : 0;
		if (serverSmsId != null && serverSmsId > 0) {
			db.execSQL("UPDATE " + SQLLite.SMS_HISTORY_TABLE_NAME + " set serverSmsId= '" + serverSmsId + "', isSyncked= '" + isSyncked
					+ "' where id = '" + sms.getId() + "'");
			Log.i("Update Offline SMS", "SMS is successfully updated in withserverSmsId");
		}
	}

	public boolean deleteOfflineSMSHistory(Long id, SQLiteDatabase db) {
		if (id != null && id > 0) {
			db.execSQL("DELETE FROM " + SQLLite.SMS_HISTORY_TABLE_NAME + " WHERE serverSmsId = " + id + "");
			return true;
		}
		return false;
	}

	public List<SMS> getAllOfflineSmses(SQLiteDatabase db) {
		Cursor cursor = db.query(SQLLite.SMS_HISTORY_TABLE_NAME, null, null, null, null, null, "substr(date, 7,4) DESC, substr(date, 4, 2) DESC, substr(date, 1,2) DESC, substr(date, 12, 2) DESC, substr(date, 15, 2) DESC, substr(date, 18,2) DESC ");
		return extractSMSesFromCursor(cursor);
	}

	public void deleteAllSmses(SQLiteDatabase db) {
		db.execSQL("DELETE FROM " + SQLLite.SMS_HISTORY_TABLE_NAME + "");
	}

	public List<SMS> getAllOfflineSmsesWithoudServerId(SQLiteDatabase db) {
		Cursor cursor = db.query(SQLLite.SMS_HISTORY_TABLE_NAME, null, " serverSmsId is null or serverSmsId=?", new String[] { "" }, null,
				null, "substr(date, 7,4) DESC, substr(date, 4, 2) DESC, substr(date, 1,2) DESC, substr(date, 12, 2) DESC, substr(date, 15, 2) DESC, substr(date, 18,2) DESC ");
		return extractSMSesFromCursor(cursor);
	}

	private SMS extractSmsFromCursor(Cursor cursor) {
		SMS sms = null;
		if (cursor.moveToFirst()) {
			sms = new SMS();
			if (cursor.getInt(0) > 0) {
				sms.setId(cursor.getInt(0));
			}
			long serverSideCallId = cursor.getLong(1);
			Long serverSMSId = Long.valueOf(serverSideCallId);
			if (serverSMSId != null && serverSMSId > 0) {
				sms.setServerSmsId(serverSMSId);
			}

			sms.setUserId(cursor.getLong(2));
			sms.setFrom(cursor.getString(3));
			sms.setMessage(cursor.getString(4));
			sms.setContactName(cursor.getString(5));

			// for date
			SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_SAVE_FORMAT);
			String date = cursor.getString(6);
			try {
				Date parsedDate = format.parse(date);
				sms.setDate(format.format(parsedDate));
			} catch (ParseException e) {
				e.printStackTrace();
			}

			sms.setSyncked(cursor.getInt(7) > 0);
			sms.setType(cursor.getInt(8));
		}
		cursor.close();
		return sms;
	}

	public SMS getSmsByDateAndType(SMS sms, SQLiteDatabase db) {
		Cursor cursor = db.rawQuery(
				"SELECT * FROM " + SQLLite.SMS_HISTORY_TABLE_NAME + " where date='" + sms.getDate() + "' and type='" + sms.getType() + "'", null);
		SMS result = extractSmsFromCursor(cursor);

		return result;
	}

	public SMS getSmsById(int id, SQLiteDatabase db) {
		Cursor cursor = db.rawQuery("SELECT * FROM " + SQLLite.SMS_HISTORY_TABLE_NAME + " where id='" + id + "'", null);
		SMS sms = extractSmsFromCursor(cursor);

		return sms;
	}

	public List<SMS> getOfflineSmsesWithoutServerIdByPagable(int currentPage, SQLiteDatabase db) {
		int offset = 0;
		if (currentPage > 0) {
			offset = SQLLite.limit * currentPage;
		}
		
		Cursor cursor = db.query(SQLLite.SMS_HISTORY_TABLE_NAME, null, " serverSmsId is null or serverSmsId=?", new String[] { "" }, null, null, "substr(date, 7,4) DESC, substr(date, 4, 2) DESC, substr(date, 1,2) DESC, substr(date, 12, 2) DESC, substr(date, 15, 2) DESC, substr(date, 18,2) DESC ", " "+ offset + ", " + SQLLite.limit);
		return extractSMSesFromCursor(cursor);
	}
}
