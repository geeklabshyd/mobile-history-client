package com.geeklabs.callbackup.sqlite;

import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.geeklabs.callbackup.domain.CallHistory;
import com.geeklabs.callbackup.domain.SMS;

public class SQLLiteManager extends SQLiteOpenHelper{
	
	private static SQLLiteManager mInstance = null;

	private static final String DATABASE_NAME = "Geeklabs_Sms_And_Call_History_db";
	private static final int DATABASE_VERSION = 1;

	private final CallRepository callRepository = new CallRepository();
	private final SMSRepository smsRepository = new SMSRepository();
	
	private SQLLiteManager(Context ctx) {
		super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public static SQLLiteManager getInstance(Context ctx) {
		// Use the application context, which will ensure that you
		// don't accidentally leak an Activity's context.
		if (mInstance == null) {
			mInstance = new SQLLiteManager(ctx.getApplicationContext());
		}
		return mInstance;
	}


	@Override
	public void onCreate(SQLiteDatabase db) {
		createCallHistoryTable(db);
		createSMSHistoryTable(db);
	}

	private void createSMSHistoryTable(SQLiteDatabase db) {
		String tableCreation = "CREATE TABLE IF NOT EXISTS " +" " + SQLLite.SMS_HISTORY_TABLE_NAME +"(" + "id INTEGER PRIMARY KEY AUTOINCREMENT," + "serverSmsId LONG,"
				+ "userId LONG, contactNumber VARCHAR(30), message VARCHAR(250), contactName VARCHAR(25), date VARCHAR(25), isSyncked BOOLEAN, type INTEGER)";
		db.execSQL(tableCreation);
	}

	private void createCallHistoryTable(SQLiteDatabase db) {
		String tableCreation = "CREATE TABLE IF NOT EXISTS " +" " + SQLLite.CALL_HISTORY_TABLE_NAME +"(" + "id INTEGER PRIMARY KEY AUTOINCREMENT," + "serverCallId LONG,"
				+ "userId LONG, number VARCHAR(25), date VARCHAR(25), time VARCHAR(25), callType INTEGER, contactName VARCHAR(25), isSyncked BOOLEAN)";
		db.execSQL(tableCreation);
	}

	//upgrade
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		// Create tables again
		onCreate(db);
	}
	//calls
	public void saveOfflineCallHistory(CallHistory call) {
		SQLiteDatabase db = this.getWritableDatabase();
		callRepository.saveCallHistory(call, db);
		//refresh adapter
	}

	public  List<CallHistory> getPaginationAllCallsByStatus(int currentPage, int type, String searchTerm) {
		return callRepository.getPaginationAllCallsByStatus(currentPage, type, getWritableDatabase(), searchTerm);
	}

	/*public List<CallHistory> getAllOfflineCalls() {
		return callRepository.getAllOfflineCalls(getWritableDatabase());
	}*/

	public void updateOfflineCallHistory(CallHistory callHistory) {
		callRepository.updateOfflineCallHistory(callHistory, getWritableDatabase());
	}

	public boolean deleteOfflineCallHistory(Long id) {
		return callRepository.deleteOfflineCallHistory(id, getWritableDatabase());
	}

	public void deleteAllCalls() {
		callRepository.deleteAllCalls(getWritableDatabase());
	}
	
	public List<CallHistory> getAllOfflineCallsWithoudServerCallId() {
		return callRepository.getAllOfflineCallsWithoudServerCallId(getWritableDatabase());
	}
	
	public CallHistory getCallHistoryById(int id) {
		return callRepository.getCallHistoryById(id, getWritableDatabase());
	}
	
	public void saveDeviceCalls(List<CallHistory> allCallsFromDevice) {
		for (CallHistory callHistory : allCallsFromDevice) {
			callRepository.saveCallHistory(callHistory, getWritableDatabase());
		}
	}

	public List<CallHistory> getOfflineCallsWithoutServerIdByPagable(int currentPageId) {
		return callRepository.getOfflineCallsWithoutServerIdByPagable(currentPageId, getWritableDatabase());
	}
	
	
	
	//sms
	public SMS getSmsById(int id) {
		return smsRepository.getSmsById(id, getWritableDatabase());
	}
	
	public void saveOfflineSMS(SMS sms) {
		smsRepository.saveOfflineSMS(sms, getWritableDatabase());
	}
	
	public  List<SMS> getPaginationAllSmses(int currentPage, String searchTerm) {
		return smsRepository.getPaginationAllSmses(currentPage, getWritableDatabase(), searchTerm);
	}
	
	public void updateOfflineSMSHistory(SMS sms) {
		smsRepository.updateOfflineSMSHistory(sms, getWritableDatabase());
	}
	
	public boolean deleteOfflineSMSHistory(Long id) {
		return smsRepository.deleteOfflineSMSHistory(id, getWritableDatabase());
	}

	public List<SMS> getAllOfflineSmses() {
		return smsRepository.getAllOfflineSmses(getWritableDatabase());
	}

	public void deleteAllSmses() {
		smsRepository.deleteAllSmses(getWritableDatabase());
	}
	public List<SMS> getAllOfflineSmsesWithoutServerId() {
		return smsRepository.getAllOfflineSmsesWithoudServerId(getWritableDatabase());
	}

	public void saveSmsFromDevice(List<SMS> smsesFromDevice) {
		for (SMS sms : smsesFromDevice) {
			smsRepository.saveOfflineSMS(sms, getWritableDatabase());
		}
	}

	public List<SMS> getOfflineSmsesWithoutServerIdByPagable(int currentPageId) {
		return smsRepository.getOfflineSmsesWithoutServerIdByPagable(currentPageId, getWritableDatabase());
	}
}
