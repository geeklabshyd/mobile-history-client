package com.geeklabs.callbackup;

import android.app.Activity;

public interface SyncService {
	
	void syncStarted(Activity activity);

	void syncCompleted();
}
